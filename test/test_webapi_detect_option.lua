# /usr/bin/env lua

--[[

  PUX API Interface library for Lua test suites

    Copyright (C) 2015 PUX Corporation. All rights reserved.

--]]

_ENV = lunit.module( "webapi_detect_option", "seeall")

local faceu   = require 'pux.webapi.faceu'
local content = require 'pux.webapi.content'
local point   = require 'pux.point'

local api
local src
local resp

function setup()
  tc:enter(_NAME)

  api = faceu.new {
    api_key = tc.api_key
  }
end

function teardown()
  tc:leave()
end

--
-- 顔サイズ(LARGE)
--
function test_detect_with_face_size_large()
  api.face_size = "LARGE"

  src = content.file( tc:path( "flickr-0002.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "flickr-0002:fs=l", resp)

  assert_true( tc:frame_check( "flickr-0002:fs=l", resp))
end

--
-- 顔サイズ(MIDDLE)
--
function test_detect_with_face_size_middle()
  api.face_size = "MIDDLE"

  src = content.file( tc:path( "flickr-0002.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "flickr-0002:fs=m", resp)

  assert_true( tc:frame_check( "flickr-0002:fs=m", resp))
end

--
-- 顔サイズ(SMALL)
--
function test_detect_with_face_size_small()
  api.face_size = "SMALL"

  src = content.file( tc:path( "flickr-0002.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "flickr-0002:fs=s", resp)

  assert_true( tc:frame_check( "flickr-0002:fs=s", resp))
end


--
-- 顔パーツ検出(一人分)
--
function test_detect_with_parts_detect()
  api.parts_detect = true

  src = content.file( tc:path( "pixabay-0001.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "pixabay-0001:fs=s:pd", resp)
  assert_true( tc:frame_check( "pixabay-0001:fs=s:pd", resp))
end

--
-- 顔パーツ検出(複数人数分)
--
function test_detect_with_parts_detect_on_multi_person()
  api.face_size    = "SMALL"
  api.parts_detect = true

  src = content.file( tc:path( "flickr-0001.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "flickr-0001:fs=s:pd", resp)
  assert_true( tc:frame_check( "flickr-0001:fs=s:pd", resp))
end


--
-- まばたき検出
--
function test_detect_with_age()
  api.blink = true

  src = content.file( tc:path( "pdpicture-0001.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "pdpicture-0001:bl", resp)
  assert_true( tc:frame_check( "pdpicture-0001:bl", resp))

  src = content.file( tc:path( "pixabay-0001.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "pixabay-0001:bl", resp)
  assert_true( tc:frame_check( "pixabay-0001:bl", resp))

  src = content.file( tc:path( "pixabay-0002.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "pixabay-0002:bl", resp)
  assert_true( tc:frame_check( "pixabay-0002:bl", resp))
end


--
-- 年齢検出のみ有効 (年齢・性別ともに出力される)
--
function test_detect_with_age()
  api.age    = true
  api.gender = false

  src = content.file( tc:path( "pdpicture-0001.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "pdpicture-0001:ag", resp)
  assert_true( tc:frame_check( "pdpicture-0001:ag", resp))

  src = content.file( tc:path( "pixabay-0001.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "pixabay-0001:ag", resp)
  assert_true( tc:frame_check( "pixabay-0001:ag", resp))

  src = content.file( tc:path( "pixabay-0002.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "pixabay-0002:ag", resp)
  assert_true( tc:frame_check( "pixabay-0002:ag", resp))
end

--
-- 性別検出のみ有効 (年齢・性別ともに出力される)
--
function test_detect_with_gender()
  api.age    = false
  api.gender = true


  src = content.file( tc:path( "pdpicture-0001.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "pdpicture-0001:gd", resp)
  assert_true( tc:frame_check( "pdpicture-0001:gd", resp))


  src = content.file( tc:path( "pixabay-0001.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "pixabay-0001:gd", resp)
  assert_true( tc:frame_check( "pixabay-0001:gd", resp))


  src = content.file( tc:path( "pixabay-0002.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "pixabay-0002:gd", resp)
  assert_true( tc:frame_check( "pixabay-0002:gd", resp))
end

--
-- 年齢・性別検出両方を有効 (年齢・性別ともに出力される)
--
function test_detect_with_age_and_gender()
  api.age     = true
  api.gender  = true

  src = content.file( tc:path( "pdpicture-0001.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "pdpicture-0001:ag:gd", resp)
  assert_true( tc:frame_check( "pdpicture-0001:ag:gd", resp))


  src = content.file( tc:path( "pixabay-0001.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "pixabay-0001:ag:gd", resp)
  assert_true( tc:frame_check( "pixabay-0001:ag:gd", resp))


  src = content.file( tc:path( "pixabay-0002.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "pixabay-0002:ag:gd", resp)
  assert_true( tc:frame_check( "pixabay-0002:ag:gd", resp))
end

--
-- 顔向き確度検出
--
function test_detect_with_angle()
  api.angle = true

  src = content.url( tc:url('wikimedia-0001'))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "wikimedia-0001:an", resp)
  assert_true( tc:frame_check( "wikimedia-0001:an", resp))


  src = content.file( tc:path( "flickr-0004.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "flickr-0004:an", resp)
  assert_true( tc:frame_check( "flickr-0004:an", resp))


  src = content.file( tc:path( "pixabay-0001.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "pixabay-0001:an", resp)
  assert_true( tc:frame_check( "pixabay-0001:an", resp))


  src = content.file( tc:path( "pixabay-0002.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "pixabay-0002:an", resp)
  assert_true( tc:frame_check( "pixabay-0002:an", resp))
end

--
-- 笑顔検出
--
function test_detect_with_smile()
  api.smile = true

  src = content.url( tc:url('wikimedia-0001'))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "wikimedia-0001:sm", resp)
  assert_true( tc:frame_check( "wikimedia-0001:sm", resp))


  src = content.file( tc:path( "flickr-0004.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "flickr-0004:sm", resp)
  assert_true( tc:frame_check( "flickr-0004:sm", resp))


  src = content.file( tc:path( "pixabay-0001.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "pixabay-0001:sm", resp)
  assert_true( tc:frame_check( "pixabay-0001:sm", resp))


  src = content.file( tc:path( "pixabay-0002.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "pixabay-0002:sm", resp)
  assert_true( tc:frame_check( "pixabay-0002:sm", resp))
end

--
-- お楽しみ機能
--
function test_detect_with_funny_option()
  api.funny_option = true

  src = content.url( tc:url('wikimedia-0001'))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "wikimedia-0001:fo", resp)
  assert_true( tc:frame_check( "wikimedia-0001:fo", resp))


  src = content.file( tc:path( "flickr-0004.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "flickr-0004:fo", resp)
  assert_true( tc:frame_check( "flickr-0004:fo", resp))


  src = content.file( tc:path( "pixabay-0001.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "pixabay-0001:fo", resp)
  assert_true( tc:frame_check( "pixabay-0001:fo", resp))


  src = content.file( tc:path( "pixabay-0002.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "pixabay-0002:fo", resp)
  assert_true( tc:frame_check( "pixabay-0002:fo", resp))
end

-- vi:set ts=2 sw=2 et:
