#! /usr/bin/env lua

--[[

  PUX API Interface library for Lua test suites
    
    Copyright (C) 2015 PUX Corporation. All rights reserved.

--]]

_ENV = lunit.module( "webapi_identify_accuracy", "seeall")

local faceu   = require 'pux.webapi.faceu'
local content = require 'pux.webapi.content'
local point   = require 'pux.point'

local api
local src
local resp

function setup()
  tc:enter(_NAME)
  -- tc:set_mode( "LOG-CORRECT")

  if not api then
    api = faceu.new {
      api_key = tc.api_key
    }

    api:unregister()
    api:register( content.url( tc:url( "pdpicture-0007")), "girl1")
    api:register( content.url( tc:url( "pdphotos-0001")), "guy1")
  end
end

function teardown()
  tc:leave()
end

local ACCURACY_THRESHOLD = 0.7

--
-- 
--
function test_identify_accuracy_test_girl_try1()
  src  = content.file( tc:path( "pdpicture-0002.jpg"))

  assert_pass( function() resp = api:who( src) end)
  tc:output( "pdpicture-0002.jpg:md=v", resp)
  assert_equal( 1, #resp, "fail pdpicture-0002")
  assert( resp[1].i12n_accuracy > ACCURACY_THRESHOLD)
end

function test_identify_accuracy_test_girl_try2()
  src  = content.file( tc:path( "pdpicture-0003.jpg"))

  assert_pass( function() resp = api:who( src) end)
  tc:output( "pdpicture-0003.jpg:md=v", resp)
  assert_equal( 1, #resp, "fail pdpicture-0003")
  assert( resp[1].i12n_accuracy > ACCURACY_THRESHOLD)
end

function test_identify_accuracy_test_girl_try3()
  skip( "識別できない, 開発者に問い合わせ中")

  src  = content.file( tc:path( "pdpicture-0004.jpg"))

  assert_pass( function() resp = api:who( src) end)
  tc:output( "pdpicture-0004.jpg:md=v", resp)
  assert_equal( 1, #resp, "fail pdpicture-0004")
  assert( resp[1].i12n_accuracy > ACCURACY_THRESHOLD)
end

function test_identify_accuracy_test_girl_try4()
  src  = content.file( tc:path( "pdpicture-0005.jpg"))

  assert_pass( function() resp = api:who( src) end)
  tc:output( "pdpicture-0005.jpg:md=v", resp)
  assert_equal( 1, #resp, "fail pdpicture-0005")
  assert( resp[1].i12n_accuracy > ACCURACY_THRESHOLD)
end

function test_identify_accuracy_test_girl_try5()
  src  = content.url( tc:url( "pdpicture-0007"))

  assert_pass( function() resp = api:who( src) end)
  tc:output( "pdpicture-0007:md=v", resp)
  assert_equal( 1, #resp, "fail pdpicture-0007")
  assert( resp[1].i12n_accuracy > ACCURACY_THRESHOLD)
end

function test_identify_accuracy_test_girl_try6()
  src  = content.url( tc:url( "pdpicture-0008"))

  assert_pass( function() resp = api:who( src) end)
  tc:output( "pdpicture-0008:md=v", resp)
  assert_equal( 1, #resp, "fail pdpicture-0008")
  assert( resp[1].i12n_accuracy > ACCURACY_THRESHOLD)
end

function test_identify_accuracy_test_girl_try7()
  skip( "識別できない, 開発者に問い合わせ中")

  src  = content.url( tc:url( "pdpicture-0009"))

  assert_pass( function() resp = api:who( src) end)
  tc:output( "pdpicture-0009:md=v", resp)
  assert_equal( 1, #resp, "fail pdpicture-0009")
  assert( resp[1].i12n_accuracy > ACCURACY_THRESHOLD)
end

function test_identify_accuracy_test_girl_try8()
  src  = content.url( tc:url( "pdpicture-0010"))

  assert_pass( function() resp = api:who( src) end)
  tc:output( "pdpicture-0010:md=v", resp)
  assert_equal( 1, #resp, "fail pdpicture-0010")
  assert( resp[1].i12n_accuracy > ACCURACY_THRESHOLD)
end

function test_identify_accuracy_test_girl_try9()
  src  = content.url( tc:url( "pdpicture-0011"))

  assert_pass( function() resp = api:who( src) end)
  tc:output( "pdpicture-0011:md=v", resp)
  assert_equal( 1, #resp, "fail pdpicture-0011")
  assert( resp[1].i12n_accuracy > ACCURACY_THRESHOLD)
end

function test_identify_accuracy_test_girl_try10()
  skip( "識別できない, 開発者に問い合わせ中")

  src  = content.url( tc:url( "pdpicture-0012"))

  assert_pass( function() resp = api:who( src) end)
  tc:output( "pdpicture-0012:md=v", resp)
  assert_equal( 1, #resp, "fail pdpicture-0012")
  assert( resp[1].i12n_accuracy > ACCURACY_THRESHOLD)
end

function test_identify_accuracy_test_girl_try11()
  src  = content.url( tc:url( "pdpicture-0013"))

  assert_pass( function() resp = api:who( src) end)
  tc:output( "pdpicture-0013:md=v", resp)
  assert_equal( 1, #resp, "fail pdpicture-0013")
  assert( resp[1].i12n_accuracy > ACCURACY_THRESHOLD)
end

function test_identify_accuracy_test_girl_try12()
  src  = content.url( tc:url( "pdpicture-0014"))

  assert_pass( function() resp = api:who( src) end)
  tc:output( "pdpicture-0014:md=v", resp)
  assert_equal( 0, #resp)
end

function test_identify_accuracy_test_guy_try1()
  src  = content.url( tc:url( "pdphotos-0002"))

  assert_pass( function() resp = api:who( src) end)
  tc:output( "pdphotos-0002:md=v", resp)
  assert_equal( 1, #resp, "fail pdphotos-0002")
  assert( resp[1].i12n_accuracy > ACCURACY_THRESHOLD)
end

function test_identify_accuracy_test_guy_try2()
  skip( "識別できない, 開発者に問い合わせ中")

  src  = content.url( tc:url( "pdphotos-0003"))

  assert_pass( function() resp = api:who( src) end)
  tc:output( "pdphotos-0003:md=v", resp)
  assert_equal( 1, #resp, "fail pdphotos-0003")
  assert( resp[1].i12n_accuracy > ACCURACY_THRESHOLD)
end

function test_identify_accuracy_test_guy_try3()
  src  = content.url( tc:url( "pdphotos-0004"))

  assert_pass( function() resp = api:who( src) end)
  tc:output( "pdphotos-0004:md=v", resp)
  assert_equal( 1, #resp, "fail pdphotos-0004")
  assert( resp[1].i12n_accuracy > ACCURACY_THRESHOLD)
end

function test_identify_accuracy_test_guy_try5()
  src  = content.url( tc:url( "pdphotos-0006"))

  assert_pass( function() resp = api:who( src) end)
  tc:output( "pdphotos-0006:md=v", resp)
  assert_equal( 1, #resp, "fail pdphotos-0006")
  assert( resp[1].i12n_accuracy > ACCURACY_THRESHOLD)
end

function test_identify_accuracy_test_guy_try6()
  src  = content.url( tc:url( "pdphotos-0007"))

  assert_pass( function() resp = api:who( src) end)
  tc:output( "pdphotos-0007:md=v", resp)
  assert_equal( 1, #resp, "fail pdphotos-0007")
  assert( resp[1].i12n_accuracy > ACCURACY_THRESHOLD)
end

function test_identify_accuracy_test_guy_try7()
  src  = content.url( tc:url( "pdphotos-0008"))

  assert_pass( function() resp = api:who( src) end)
  tc:output( "pdphotos-0008:md=v", resp)
  assert_equal( 1, #resp, "fail pdphotos-0008")
  assert( resp[1].i12n_accuracy > ACCURACY_THRESHOLD)
end

function test_identify_accuracy_test_guy_try8()
  src  = content.url( tc:url( "pdphotos-0009"))

  assert_pass( function() resp = api:who( src) end)
  tc:output( "pdphotos-0009:md=v", resp)
  assert_equal( 1, #resp, "fail pdphotos-0009")
  assert( resp[1].i12n_accuracy > ACCURACY_THRESHOLD)
end

function test_identify_accuracy_test_guy_try9()
  skip( "識別できない, 開発者に問い合わせ中")

  src  = content.url( tc:url( "pdphotos-0010"))

  assert_pass( function() resp = api:who( src) end)
  tc:output( "pdphotos-0010:md=v", resp)
  assert_equal( 1, #resp, "fail pdphotos-0010")
  assert( resp[1].i12n_accuracy > ACCURACY_THRESHOLD)
end

function test_identify_accuracy_test_guy_try10()
  skip( "識別できない, 開発者に問い合わせ中")

  src  = content.url( tc:url( "pdphotos-0011"))

  assert_pass( function() resp = api:who( src) end)
  tc:output( "pdphotos-0011:md=v", resp)
  assert_equal( 1, #resp, "fail pdphotos-0011")
  assert( resp[1].i12n_accuracy > ACCURACY_THRESHOLD)
end

function test_identify_accuracy_test_guy_try11()
  src  = content.url( tc:url( "pdphotos-0012"))

  assert_pass( function() resp = api:who( src) end)
  tc:output( "pdphotos-0012:md=v", resp)
  assert_equal( 1, #resp, "fail pdphotos-0012")
  assert( resp[1].i12n_accuracy > ACCURACY_THRESHOLD)
end

function test_identify_accuracy_test_guy_try12()
  src  = content.url( tc:url( "pdphotos-0013"))

  assert_pass( function() resp = api:who( src) end)
  tc:output( "pdphotos-0013:md=v", resp)
  assert_equal( 1, #resp, "fail pdphotos-0013")
  assert( resp[1].i12n_accuracy > ACCURACY_THRESHOLD)
end

function test_identify_accuracy_test_guy_try13()
  skip( "識別できない, 開発者に問い合わせ中")

  src  = content.url( tc:url( "pdphotos-0014"))

  assert_pass( function() resp = api:who( src) end)
  tc:output( "pdphotos-0014:md=v", resp)
  assert_equal( 1, #resp, "fail pdphotos-0014")
  assert( resp[1].i12n_accuracy > ACCURACY_THRESHOLD)
end

function test_identify_accuracy_test_guy_try14()
  skip( "識別できない, 開発者に問い合わせ中")

  src  = content.url( tc:url( "pdphotos-0015"))

  assert_pass( function() resp = api:who( src) end)
  tc:output( "pdphotos-0015:md=v", resp)
  assert_equal( 1, #resp, "fail pdphotos-0015")
  assert( resp[1].i12n_accuracy > ACCURACY_THRESHOLD)
end

-- vi:set ts=2 sw=2 et:
