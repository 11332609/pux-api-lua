#! /usr/bin/env lua

--[[

  PUX API Interface library for Lua test suites
    
    Copyright (C) 2015 PUX Corporation. All rights reserved.

--]]

_ENV = lunit.module( "webapi_register_number_of_person", "seeall")

local faceu   = require 'pux.webapi.faceu'
local content = require 'pux.webapi.content'
local point   = require 'pux.point'

local api
local src
local resp

function setup()
  tc:enter(_NAME)

  if not api then
    api = faceu.new {
      api_key   = tc.api_key,
      face_size = "SMALL"
    }

    api:unregister()
  end
end

function teardown()
  tc:leave()
end

--
-- 0人
--
function test_register_no_persons()
  src  = content.file( tc:path( "pixabay-0005.jpg"))
  resp = api:register( src)

  tc:output( "pixabay-0005:md:=r:fs=s", resp)
  assert_equal( 0, #resp)
end

--
-- 6人
--
function test_register_6_persons()
  src  = content.file( tc:path( "pixabay-0004.jpg"))
  resp = api:register( src)

  tc:output( "pixabay-0004:md=r:fs=s", resp)
  assert_equal( 6, #resp)
end

--
-- 13人
--
function test_register_13_persons()
  src  = content.file( tc:path( "flickr-0001.jpg"))
  resp = api:register( src)

  tc:output( "flickr-0001:md=r:fs=s", resp)
  assert_equal( 13, #resp)
end

-- vi:set ts=2 sw=2 et:
