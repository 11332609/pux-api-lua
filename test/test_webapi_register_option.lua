# /usr/bin/env lua

--[[

  PUX API Interface library for Lua test suites

    Copyright (C) 2015 PUX Corporation. All rights reserved.

--]]

_ENV = lunit.module( "webapi_register_option", "seeall")

local faceu   = require 'pux.webapi.faceu'
local content = require 'pux.webapi.content'
local point   = require 'pux.point'

local api
local src
local resp

function setup()
  tc:enter(_NAME)

  if not api then
    api = faceu.new {
      api_key = tc.api_key
    }

    api:unregister()
  end

  api.face_size    = "MIDDLE"
  api.parts_detect = false
  api.blink        = false
  api.age          = false
  api.gender       = false
  api.smile        = false
  api.angle        = false
  api.funny_option = false
end

function teardown()
  tc:leave()
end

--
-- 顔サイズ(LARGE)
--
function test_register_with_face_size_large()
  api.face_size = "LARGE"

  src = content.file( tc:path( "flickr-0002.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "flickr-0002:md=r:fs=l", resp)

  assert_true( tc:frame_check( "flickr-0002:md=r:fs=l", resp))
end

--
-- 顔サイズ(MIDDLE)
--
function test_register_with_face_size_middle()
  api.face_size = "MIDDLE"

  src = content.file( tc:path( "flickr-0002.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "flickr-0002:md=r:fs=m", resp)

  assert_true( tc:frame_check( "flickr-0002:md=r:fs=m", resp))
end

--
-- 顔サイズ(SMALL)
--
function test_register_with_face_size_small()
  api.face_size = "SMALL"

  src = content.file( tc:path( "flickr-0002.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "flickr-0002:md=r:fs=s", resp)

  assert_true( tc:frame_check( "flickr-0002:md=r:fs=s", resp))
end


--
-- 顔パーツ検出(一人分)
--
function test_register_with_parts_detect()
  api.parts_detect = true

  src = content.file( tc:path( "pixabay-0001.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "pixabay-0001:md=r:fs=s:pd", resp)
  assert_true( tc:frame_check( "pixabay-0001:md=r:fs=s:pd", resp))
end

--
-- 顔パーツ検出(複数人数分)
--
function test_register_with_parts_detect_on_multi_person()
  api.face_size    = "SMALL"
  api.parts_detect = true

  src = content.file( tc:path( "flickr-0001.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "flickr-0001:md=r:fs=s:pd", resp)
  assert_true( tc:frame_check( "flickr-0001:md=r:fs=s:pd", resp))
end


--
-- まばたき検出
--
function test_register_with_age()
  api.blink = true

  src = content.file( tc:path( "pdpicture-0001.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "pdpicture-0001:md=r:bl", resp)
  assert_true( tc:frame_check( "pdpicture-0001:md=r:bl", resp))

  src = content.file( tc:path( "pixabay-0001.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "pixabay-0001:md=r:bl", resp)
  assert_true( tc:frame_check( "pixabay-0001:md=r:bl", resp))

  src = content.file( tc:path( "pixabay-0002.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "pixabay-0002:md=r:bl", resp)
  assert_true( tc:frame_check( "pixabay-0002:md=r:bl", resp))
end


--
-- 年齢検出のみ有効 (年齢・性別ともに出力される)
--
function test_register_with_age()
  api.age    = true
  api.gender = false

  src = content.file( tc:path( "pdpicture-0001.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "pdpicture-0001:md=r:ag", resp)
  assert_true( tc:frame_check( "pdpicture-0001:md=r:ag", resp))

  src = content.file( tc:path( "pixabay-0001.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "pixabay-0001:md=r:ag", resp)
  assert_true( tc:frame_check( "pixabay-0001:md=r:ag", resp))

  src = content.file( tc:path( "pixabay-0002.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "pixabay-0002:md=r:ag", resp)
  assert_true( tc:frame_check( "pixabay-0002:md=r:ag", resp))
end

--
-- 性別検出のみ有効 (年齢・性別ともに出力される)
--
function test_register_with_gender()
  api.age    = false
  api.gender = true


  src = content.file( tc:path( "pdpicture-0001.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "pdpicture-0001:md=r:gd", resp)
  assert_true( tc:frame_check( "pdpicture-0001:md=r:gd", resp))


  src = content.file( tc:path( "pixabay-0001.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "pixabay-0001:md=r:gd", resp)
  assert_true( tc:frame_check( "pixabay-0001:md=r:gd", resp))


  src = content.file( tc:path( "pixabay-0002.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "pixabay-0002:md=r:gd", resp)
  assert_true( tc:frame_check( "pixabay-0002:md=r:gd", resp))
end

--
-- 年齢・性別検出両方を有効 (年齢・性別ともに出力される)
--
function test_register_with_age_and_gender()
  api.age     = true
  api.gender  = true

  src = content.file( tc:path( "pdpicture-0001.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "pdpicture-0001:md=r:ag:gd", resp)
  assert_true( tc:frame_check( "pdpicture-0001:md=r:ag:gd", resp))


  src = content.file( tc:path( "pixabay-0001.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "pixabay-0001:md=r:ag:gd", resp)
  assert_true( tc:frame_check( "pixabay-0001:md=r:ag:gd", resp))


  src = content.file( tc:path( "pixabay-0002.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "pixabay-0002:md=r:ag:gd", resp)
  assert_true( tc:frame_check( "pixabay-0002:md=r:ag:gd", resp))
end

--
-- 顔向き確度検出
--
function test_register_with_angle()
  api.angle = true

  src = content.url( tc:url('wikimedia-0001'))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "wikimedia-0001:md=r:an", resp)
  assert_true( tc:frame_check( "wikimedia-0001:md=r:an", resp))


  src = content.file( tc:path( "flickr-0004.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "flickr-0004:md=r:an", resp)
  assert_true( tc:frame_check( "flickr-0004:md=r:an", resp))


  src = content.file( tc:path( "pixabay-0001.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "pixabay-0001:md=r:an", resp)
  assert_true( tc:frame_check( "pixabay-0001:md=r:an", resp))


  src = content.file( tc:path( "pixabay-0002.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "pixabay-0002:md=r:an", resp)
  assert_true( tc:frame_check( "pixabay-0002:md=r:an", resp))
end

--
-- 笑顔検出
--
function test_register_with_smile()
  api.smile = true

  src = content.url( tc:url('wikimedia-0001'))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "wikimedia-0001:md=r:sm", resp)
  assert_true( tc:frame_check( "wikimedia-0001:md=r:sm", resp))


  src = content.file( tc:path( "flickr-0004.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "flickr-0004:md=r:sm", resp)
  assert_true( tc:frame_check( "flickr-0004:md=r:sm", resp))


  src = content.file( tc:path( "pixabay-0001.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "pixabay-0001:md=r:sm", resp)
  assert_true( tc:frame_check( "pixabay-0001:md=r:sm", resp))


  src = content.file( tc:path( "pixabay-0002.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "pixabay-0002:md=r:sm", resp)
  assert_true( tc:frame_check( "pixabay-0002:md=r:sm", resp))
end

--
-- お楽しみ機能
--
function test_register_with_funny_option()
  api.funny_option = true

  src = content.url( tc:url('wikimedia-0001'))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "wikimedia-0001:md=r:fo", resp)
  assert_true( tc:frame_check( "wikimedia-0001:md=r:fo", resp))


  src = content.file( tc:path( "flickr-0004.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "flickr-0004:md=r:fo", resp)
  assert_true( tc:frame_check( "flickr-0004:md=r:fo", resp))


  src = content.file( tc:path( "pixabay-0001.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "pixabay-0001:md=r:fo", resp)
  assert_true( tc:frame_check( "pixabay-0001:md=r:fo", resp))


  src = content.file( tc:path( "pixabay-0002.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "pixabay-0002:md=r:fo", resp)
  assert_true( tc:frame_check( "pixabay-0002:md=r:fo", resp))
end

-- vi:set ts=2 sw=2 et:
