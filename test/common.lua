#! /usr/bin/env lua

--[[

  PUX API interfacfe library for Lua test suites common library

    Copyright (C) 2015 PUX Corporation. All rights reserved.

--]]

local posix  = require 'posix'
local dbg    = require 'dbg_util'

local url_db = {}

---
-- @classmod test
--

---
-- @lfunction read_file
--
local function read_file( path)
  local ret
  local fp

  fp  = io.open( path)
  ret = fp:read( "*a")
  fp:close()

  return ret
end

---
-- @lfunction write_file
--
local function write_file( path, data)
  local ret
  local fp

  fp  = io.open( path, "wb")
  ret = fp:write( data)
  fp:close()

  return ret
end

---
-- @lfunction make_path
--
local function make_path( self, type, ...)
  return string.format( "%s/%s/%s",
              self.prefix, type:lower(), table.concat( {...}, "/"))
end

---
-- @function path
--
local function path( self, file)
  return make_path( self, "data", file)
end

---
-- @function read
--
local function read( self, file)
  return read_file( make_path( self, "data", file))
end


---
-- @lfunction merge_config
--
local function include( self, path)
  local func
  local key
  local val

  func = load( "return" .. read_file( path))

  for key,val in pairs( func()) do
    self[key] = val
  end
end

---
-- @lfunction
--
local function make_indent( n)
  local i
  local ret

  ret = ""

  for i = 1,n do
    ret = ret .. "  "
  end

  return ret
end

---
-- @lfunction is_ignore_field
--
function is_ignore_field( opt, name)
  local ret = false
  local i
  local val

  if opt.ignore_fields then
    for i,val in ipairs(opt.ignore_fields) do
      if name:match( val) then
        ret = true
        break
      end
    end
  end

  return ret
end

---
-- @lfunction key_regulize
--
local function key_regulize( key)
  local ret

  if key:match( "^[%l%u][%w_]*$") then
    ret = key
  else
    ret = string.format( "[%q]", key):gsub( "\n", "n")
  end

  return ret
end

---
-- @lfunction dump_table
--
local function dump_table( self, tbl, opt)
  local ret
  local tmp
  local indent
  local key
  local val
  local i

  tmp = {}

  if opt.with_indent then
    opt.indent_level = opt.indent_level + 1
  end

  for key,val in pairs(tbl) do
    if type(key) == "number" then
      if opt.ignore_index then
        val = self:dump( val, opt)
      else
        val = string.format( "[%d]=%s", key, self:dump( val, opt))
      end
    else
      if is_ignore_field(opt, key) then
        val = string.format( "%s=nil", key_regulize( key))
      else
        val = string.format( "%s=%s", key_regulize( key), self:dump( val, opt))
      end
    end

    tmp[#tmp+1] = val
  end

  table.sort( tmp)

  if opt.with_indent then
      opt.indent_level = opt.indent_level - 1

      if #tmp == 0 then
        ret = "{}"
      else
        indent = make_indent( opt.indent_level)

        ret = "{\n"

        for i,val in ipairs(tmp) do
          ret = ret .. indent .. "  " .. val .. (i < #tmp and ",\n" or "\n")
        end

        ret = ret .. indent .. "}"
      end

  else
      ret = "{" .. table.concat( tmp, ",") .. "}"
  end

  return ret
end

---
-- @lfunction dump_number
--
local function dump_number( self, val, opt)
  return (math.ceil(val) == val) and string.format( "%d", val) or tostring(val)
end

---
-- @lfunction dump_string
--
local function dump_string( self, val, opt)
  return string.format( "%q", val):gsub( "\n", "n")
end

---
-- @function dump
--
local function dump( self, val, opt)
  local typ
  local ret

  typ = type(val)
  opt = opt or {}

  if opt.with_indent and not opt.indent_level then
    opt.indent_level = 0
  end

  if type(opt.ignore_fields) == "string" then
    opt.ignore_fields = { opt.ignore_fields }
  end

  ret =
    (typ == "number")  and dump_number( self, val, opt) or
    (typ == "string")  and dump_string( self, val, opt) or
    (typ == "nil")     and "nil" or
    (typ == "boolean") and tostring( val) or
    (typ == "table")   and dump_table( self, val, opt) or
    error( 'Not supprt data type "' .. typ .. '" found.')

  return ret
end

---
-- 引数で与えられた二つのオブジェクトの比較を行います。
-- テーブルに対しては再帰的に探査し、ディープコンペアを行います。
--
-- @function compare
--
-- @param val1
--   比較対象を指定します。
--
-- @param val2
--   比較対象を指定します。
--
-- @tparam[opt] opt
--   比較時のオプション指定を行います。
--   `ignore`に無視するフィールドのリストを設定できます。
--
-- @return 
--   二つのオブジェクトが一致している場合はtrueを、そうでない場合はfalseを返し
--   ます。
--
-- @raise
--   - データ中にfunction,userdata,threadのいずれかが見つかった場合に例外が発
--     生します(Not supprt data type found)
--
-- @usage
--    -- face_idフィールドを無視するようになります
--    tc:compare( val1, val2, { ignore = "face_id" })
--
local function compare( self, val1, val2, opt)
  local dump_opt

  opt  = opt or {}

  if type(opt.ignore) == "string" then
    opt.ignore = { opt.ignore }
  end

  dump_opt = {
    ignore_index  = true,
    ignore_fields = opt.ignore
  }

  return self:dump( val1, dump_opt) == self:dump( val2, dump_opt)
end

---
-- @function master
--
local function master( self, id)
  local ret
  local path

  if self.current then
    path = make_path( self, "master", self.current)
  else
    path = make_path( self, "master", "unknown")
  end

  stat = posix.stat( path)
  if not stat then
    posix.mkdir( path)

  elseif stat.type ~= 'directory' then
    error( string.format( "%s is already exist. can't write data."))
  end

  path = string.format( "%s/%s.lua", path, id)
  ret  = load( "return " .. read_file( path))()

  return ret
end

---
-- @function check
--
local function check( self, id, resp, opt)
  return (self.mode == "LOG-CORRECT") or
                  self:compare( self:master( id), resp, opt)
end

---
-- @function output
--
local function output( self, id, resp)
  local data
  local path
  local stat

  if self.current then
    path = make_path( self, "output", self.current)
  else
    path = make_path( self, "output", "unknown")
  end

  stat = posix.stat( path)
  if not stat then
    posix.mkdir( path)

  elseif stat.type ~= 'directory' then
    error( string.format( "%s is already exist. can't write data."))
  end

  data = self:dump( resp, { ignore_index = true, with_indent = true})
  path = string.format( "%s/%s.lua", path, id)

  write_file( path, data)
end


---
-- @lfunction summarize_lv2
--
local function summarize_lv2( tbl)
  local ret
  local i
  local n

  ret = {}
  n   = 1     

  if #tbl == 0 then
    return ret
  end

  ret[1] = tbl[1]

  for i=2,#tbl do
    if ret[#ret] ~= tbl[i] then
      if n > 1 then
        ret[#ret] = string.format( "%d@%s", n, ret[#ret])
      end

      ret[#ret+1] = tbl[i]
      n = 0
    end

    n = n + 1
  end

  if n > 1 then
    ret[#ret] = string.format( "%d@%s", n, ret[#ret])
  end

  return ret
end

---
-- @lfunction summarize_lv3
--
local function summarize_lv3( tbl)
  local ret
  local i
  local n

  ret = {}
  n   = 1     

  if #tbl == 0 then
    return ret
  end

  ret[1] = tbl[1]

  for i=2,#tbl do
    if ret[#ret] ~= tbl[i] then
      if n > 1 then
        ret[#ret] = "@" .. ret[#ret]
      end

      ret[#ret+1] = tbl[i]
      n = 0
    end

    n = n + 1
  end

  if n > 1 then
    ret[#ret] = "@" .. ret[#ret]
  end

  return ret
end

---
-- @lfunction summarize_lv4
--
local function summarize_lv4( tbl)
  local ret
  local i

  ret  = {}

  if #tbl == 0 then
    return ret
  end

  ret[1] = tbl[1]

  for i=2,#tbl do
    if ret[#ret] ~= tbl[i] then
      ret[#ret+1] = tbl[i]
      n = 0
    end
  end

  return ret
end

---
-- @function frame_dump
--
local function frame_dump( self, tbl, opt)
  local ret
  local key
  local val
  local tmp
  local data


  if type(tbl) ~= "table" then
    error( "Specified data is not table.")
  end

  tmp = {}
  opt = opt or {}

  if not opt.abstruct_level then
    opt.abstruct_level = 2
  end

  if opt.indent_level then
    opt.indent_level = opt.indent_level + 1

  elseif opt.with_indent then
    opt.indent_level = 1
  end

  if type(opt.ignore_fields) == "string" then
    opt.ignore_fields = { opt.ignore_fields }
  end

  for key,val in pairs(tbl) do
    if type(key) == "number" then
      data = (opt.abstruct_level >= 1) and "" or ("[" .. key .. "]=")
    else
      data = key .. "="
    end

    if type(key) == "number" or (not is_ignore_field( opt, key)) then
      if type(val) == "table" then
        data = data .. self:frame_dump( val, opt)

      elseif opt.separate_integer and type(val) == "number" then
        data = data .. ((math.ceil(val) == val) and "integer" or "number")

      else
        data = data .. type(val)
      end

    else
      data = data .. "?"
    end

    tmp[#tmp+1] = data
  end

  table.sort( tmp)

  tmp = 
    (opt.abstruct_level <= 1) and tmp or
    (opt.abstruct_level == 2) and summarize_lv2( tmp) or
    (opt.abstruct_level == 3) and summarize_lv3( tmp) or
    (opt.abstruct_level >= 4) and summarize_lv4( tmp)

  if opt.with_indent then
      opt.indent_level = opt.indent_level - 1

      if #tmp == 0 then
        ret = "{}"
      else
        indent = make_indent( opt.indent_level)

        ret = "{\n"

        for i,val in ipairs(tmp) do
          ret = ret .. indent .. "  " .. val .. (i < #tmp and ",\n" or "\n")
        end

        ret = ret .. indent .. "}"
      end
  else
    ret = "{" .. table.concat(tmp, ",") .. "}"
  end

  return ret
end

---
-- @function frame_compare
--
local function frame_compare( self, val1, val2, opt)
  local dump_opt

  opt  = opt or {}

  if type(opt.ignore) == "string" then
    opt.ignore = { opt.ignore }
  end

  dump_opt = {
    abstruct_level   = 2,
    separate_integer = true,
    ignore_fields    = opt.ignore
  }

  return self:frame_dump( val1, opt) == self:frame_dump( val2, opt)
end

---
-- @function frame_check
--
local function frame_check( self, id, resp, opt)
  return (self.mode == "LOG-CORRECT") or
              self:frame_compare( self:master( id), resp, opt)
end

---
-- @function url
--
local function url( self, id)
  return url_db[id]
end

---
-- @function start
--
local function enter( self, name)
  self.current = name
end

---
-- @function exit
--
local function leave( self)
  self.current = ""
end

---
-- @lfunction
--
local function set_mode( self, mode)
  self.mode = mode:upper()
end

---
-- @function clear
--
local function clear( self)
  self.count = 0
end

---
-- @function up
--
local function up( self)
  self.count = self.count + 1
  return self.count == 1
end

---
-- @function down
--
local function down( self)
  self.count = self.count - 1
  return self.count == 0
end

---
-- 提供するオブジェクトを生成
--
common = {
  prefix        = posix.dirname( posix.realpath( arg[0])),
  mode          = "TEST-OPERATION",
  cuurent       = "",
  count         = 0,

  clear         = clear,
  up            = up,
  down          = down,

  enter         = enter,
  leave         = leave,
  set_mode      = set_mode,

  path          = path,
  url           = url,
  read          = read,
  master        = master,
  dump          = dump,
  compare       = compare,
  check         = check,
  output        = output,
  frame_dump    = frame_dump,
  frame_compare = frame_compare,
  frame_check   = frame_check,
}

include( common, common.prefix .. "/config.lua")
include( url_db, common.prefix .. "/data/url_db.lua")

return common

-- vi:set ts=2 sw=2 et:
