#! /usr/bin/env lua

--[[

  PUX API Interface library for Lua test suites
    
    Copyright (C) 2015 PUX Corporation. All rights reserved.

--]]

_ENV = lunit.module( "webapi_detect_number_of_person", "seeall")

local faceu   = require 'pux.webapi.faceu'
local content = require 'pux.webapi.content'
local point   = require 'pux.point'

local api
local src
local resp

function setup()
  tc:enter(_NAME)

  api = faceu.new {
    api_key   = tc.api_key,
    face_size = "SMALL"
  }
end

function teardown()
  tc:leave()
end

--
-- 0人
--
function test_detect_no_persons()
  src  = content.file( tc:path( "pixabay-0005.jpg"))
  resp = api:detect( src)

  tc:output( "pixabay-0005:fs=s", resp)
  assert_equal( 0, #resp)
end

--
-- 6人
--
function test_detect_6_persons()
  src  = content.file( tc:path( "pixabay-0004.jpg"))
  resp = api:detect( src)

  tc:output( "pixabay-0004:fs=s", resp)
  assert_equal( 6, #resp)
end

--
-- 13人
--
function test_detect_13_persons()
  src  = content.file( tc:path( "flickr-0001.jpg"))
  resp = api:detect( src)

  tc:output( "flickr-0001:fs=s", resp)
  assert_equal( 13, #resp)
end


-- vi:set ts=2 sw=2 et:
