#! /usr/bin/env lua

--[[

  PUX API Interface library for Lua test suites
    
    Copyright (C) 2015 PUX Corporation. All rights reserved.

--]]

_ENV = lunit.module( "webapi_put_tag_target_select", "seeall")

local faceu   = require 'pux.webapi.faceu'
local content = require 'pux.webapi.content'
local point   = require 'pux.point'

local api
local src
local resp

local opt = {
  ignore = "registered_image"
}

function setup()
  tc:enter(_NAME)
  -- tc:set_mode( "LOG-CORRECT")

  if not api then
    api = faceu.new {
      api_key = tc.api_key
    }

    api:unregister()

    -- faceId:1
    tags = {
      group = "A",
      klass = "Z",
      name  = "pic-001",
      "test1"
    }
    api:register( content.file( tc:path( "pixabay-0001.jpg")), tags)

    -- faceId:2
    tags = {
      group = "A",
      klass = "Y",
      name  = "pic-002",
      "test1",
      "test2"
    }
    api:register( content.file( tc:path( "pixabay-0001.jpg")), tags)

    -- faceId:3
    tags = {
      group = "B",
      klass = "Y",
      name  = "pic-003",
      "test1",
      "test2"
    }
    api:register( content.file( tc:path( "pixabay-0001.jpg")), tags)

    -- faceId:4
    tags = {
      group = "C",
      klass = "Y",
      name  = "pic-004",
      "test2"
    }
    api:register( content.file( tc:path( "pixabay-0001.jpg")), tags)

    -- faceId:5
    tags = {
      group = "C",
      klass = "Z",
      name  = "pic-005",
      "test3"
    }
    api:register( content.file( tc:path( "pixabay-0001.jpg")), tags)

    -- faceId:6
    tags = {
      group = "D",
      klass = "Z",
      name  = "pic-006",
      "test3"
    }
    api:register( content.file( tc:path( "pixabay-0001.jpg")), tags)

    -- faceId:7 & faceId:8
    tags = {
      group = "D",
      klass = "Z",
      name  = "pic-007",
      "test3"
    }
    api:register( content.url( tc:url( "pdpicture-0014")), tags)
  end
end

function teardown()
  api:remove_tag( "mark", "mark")
  tc:leave()
end

--
--  タグが含まれているかどうかで選択
--
function test_webapi_put_tag_select_by_tag_includes()
  cond = "test1"

  api:put_tag( cond, "mark")

  resp = api:list()
    
  tc:output( "select_by_tag_includes", resp)
  assert( tc:check( "select_by_tag_includes", resp, opt))
end

--
--  複数のタグが含まれているかどうか(AND条件となる)
--
function test_webapi_put_tag_select_by_multi_tag_includes()
  cond = {
    "test1",
    "test2",
  }

  api:put_tag( cond, "mark")

  resp = api:list()

  tc:output( "select_by_multi_tag_includes", resp)
  assert( tc:check( "select_by_multi_tag_includes", resp, opt))
end

--
-- タグの値で選択
--
function test_webapi_put_tag_select_by_tag_value()
  cond =  {
    group = "A"
  }

  api:put_tag( cond, "mark")

  resp = api:list()

  tc:output( "select_by_tag_value", resp)
  assert( tc:check( "select_by_tag_value", resp, opt))
end

--
--  キーの異なる複数のタグの値で選択(AND条件となる)
--
function test_webapi_put_tag_select_by_multi_tag_value()
  cond = {
    group = "A",
    klass = "Z"
  }

  api:put_tag( cond, "mark")

  resp = api:list()

  tc:output( "select_by_multi_tags_value", resp)
  assert( tc:check( "select_by_multi_tags_value", resp, opt))
end



-- vi:set ts=2 sw=2 et:
