#! /usr/bin/env lua

--[[

  PUX API Interface library for Lua test suites
    
    Copyright (C) 2015 PUX Corporation. All rights reserved.

--]]

_ENV = lunit.module( "webapi_identify_target_select", "seeall")

local faceu   = require 'pux.webapi.faceu'
local content = require 'pux.webapi.content'
local point   = require 'pux.point'

local api
local src
local resp
local cond

function setup()
  local tags
  tc:enter(_NAME)
  -- tc:set_mode( "LOG-CORRECT")

  if not api then
    api = faceu.new {
      api_key = tc.api_key
    }

    api:unregister()

    -- faceId:1
    tags = {
      group = "A",
      klass = "Z",
      name  = "pic-001",
      "test1"
    }
    api:register( content.file( tc:path( "pixabay-0001.jpg")), tags)

    -- faceId:2
    tags = {
      group = "A",
      klass = "Y",
      name  = "pic-002",
      "test1",
      "test2"
    }
    api:register( content.file( tc:path( "pixabay-0001.jpg")), tags)

    -- faceId:3
    tags = {
      group = "B",
      klass = "Y",
      name  = "pic-003",
      "test1",
      "test2"
    }
    api:register( content.file( tc:path( "pixabay-0001.jpg")), tags)

    -- faceId:4
    tags = {
      group = "C",
      klass = "Y",
      name  = "pic-004",
      "test2"
    }
    api:register( content.file( tc:path( "pixabay-0001.jpg")), tags)

    -- faceId:5
    tags = {
      group = "C",
      klass = "Z",
      name  = "pic-005",
      "test3"
    }
    api:register( content.file( tc:path( "pixabay-0001.jpg")), tags)

    -- faceId:6
    tags = {
      group = "D",
      klass = "Z",
      name  = "pic-006",
      "test3"
    }
    api:register( content.file( tc:path( "pixabay-0001.jpg")), tags)

    -- faceId:7 & faceId:8
    tags = {
      group = "D",
      klass = "Z",
      name  = "pic-007",
      "test3"
    }
    api:register( content.url( tc:url( "pdpicture-0014")), tags)
  end
end

function teardown()
  tc:leave()
end

--
--  タグが含まれているかどうかで選択
--
function test_webapi_identify_select_by_tag_includes()
  cond = "test1"
  resp = api:who( content.file( tc:path( "pixabay-0001.jpg")), cond)

  tc:output( "select_by_tag_includes", resp)
  assert_equal( 3, #resp[1].candidates)
  assert( tc:check( "select_by_tag_includes", resp))
end

--
--  複数のタグが含まれているかどうか(AND条件となる)
--
function test_webapi_identify_select_by_multi_tag_includes()
  cond = {
    "test1",
    "test2"
  }

  resp = api:who( content.file( tc:path( "pixabay-0001.jpg")), cond)

  tc:output( "select_by_multi_tag_includes", resp)
  assert_equal( 2, #resp[1].candidates)
  assert( tc:check( "select_by_multi_tag_includes", resp))
end

--
-- タグの値で選択
--
function test_webapi_identify_select_by_tag_value()
  cond = {
    group = "A"
  }

  resp = api:who( content.file( tc:path( "pixabay-0001.jpg")), cond)

  tc:output( "select_by_tag_value", resp)
  assert_equal( 2, #resp[1].candidates)
  assert( tc:check( "select_by_tag_value", resp))
end

--
-- キーの異なる複数のタグの値で選択(AND条件となる)
--
function test_webapi_identify_select_by_multi_tag_value()
  cond = {
    group = "A",
    klass = "Z"
  }

  resp = api:who( content.file( tc:path( "pixabay-0001.jpg")), cond)

  tc:output( "select_by_multi_tags_value", resp)
  assert_equal( 1, #resp[1].candidates)
  assert( tc:check( "select_by_multi_tags_value", resp))
end

--
-- キーが同じ複数のタグの値で選択(OR条件となる)
--
function test_webapi_identify_select_by_multi_tags_of_same_key()
  cond = {
    group = { "A", "B"}
  }

  resp = api:who( content.file( tc:path( "pixabay-0001.jpg")), cond)

  tc:output( "select_by_multi_tags_of_same_key", resp)
  assert_equal( 3, #resp[1].candidates)
  assert( tc:check( "select_by_multi_tags_of_same_key", resp))
end



--
-- faceIdで指定 (認証ではfaceIdでの選択は未サポート)
--
function test_webapi_identify_select_by_face_id()
  cond = 4
  src  = content.file( tc:path( "pixabay-0001.jpg"))

  assert_error( function() api:who( src, cond) end)


  cond = { "test", 4 }
  src  = content.file( tc:path( "pixabay-0001.jpg"))

  assert_error( function() api:who( src, cond) end)


  cond = { group = "A", 4 }
  src  = content.file( tc:path( "pixabay-0001.jpg"))

  assert_error( function() api:who( src, cond) end)
end

--
-- 複合条件
--
function test_webapi_identify_select_by_complex_condition_1()
  cond = {
    klass = "Y",
    "test2"
  }

  resp = api:who( content.file( tc:path( "pixabay-0001.jpg")), cond)

  tc:output( "select_by_complex_condition_1", resp)
  assert_equal( 3, #resp[1].candidates)
  assert( tc:check( "select_by_complex_condition_1", resp))
end

function test_webapi_identify_select_by_complex_condition_2()
  cond = {
    group = { "B", "C"},
    klass = "Y",
  }

  resp = api:who( content.file( tc:path( "pixabay-0001.jpg")), cond)

  tc:output( "select_by_complex_condition_2", resp)
  assert_equal( 2, #resp[1].candidates)
  assert( tc:check( "select_by_complex_condition_2", resp))
end

function test_webapi_identify_select_by_complex_condition_3()
  cond = {
    group = { "B", "C"},
    klass = "Y",
    "test1"
  }

  resp = api:who( content.file( tc:path( "pixabay-0001.jpg")), cond)

  tc:output( "select_by_complex_condition_3", resp)
  assert_equal( 1, #resp[1].candidates)
  assert( tc:check( "select_by_complex_condition_3", resp))
end


-- vi:set ts=2 sw=2 et:
