#! /usr/bin/env lua

--[[

  PUX API library for Lua test driver

    Copyright (C) 2015 PUX Corporation. All rights reserved.

--]]

--
-- 共通で使用するライブラリの読み込み
-- 全体で共通で使う為、グローバル変数で扱います
--
lunit          = require 'lunitx'
lunit.console  = require 'lunit.console'

tc             = require 'test.common'
dbg            = require 'dbg_util'

-- tc:set_mode( "LOG-CORRECT")

--
-- "test_*.lua"にマッチするファイルを検索し実行する
--
local posix = require 'posix'
local i
local file
local func

for i,file in ipairs( posix.glob( tc.prefix .. "/test_*.lua")) do
  dofile( file)
end

-- vi:set ts=2 sw=2 et:
