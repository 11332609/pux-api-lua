# 素材入手元

## ぱくたそ
[![ぱくたそ](http://www.pakutaso.com/shared/img/site/logo_chara.gif)](http://www.pakutaso.com)

### 利用条件のメモ

 詳細は[こちら](http://www.pakutaso.com/userpolicy.html)

  - クレジット表記が必要になる場合がある。該当するのは以下の場合。
    - 販売物
    - 写真素材を第三者が利用可能な状態で配布する場合。

  二時配布を行う場合は、「ぱくたそ」の利用規約に従う旨をユーザに要求する必要が    ある。

### 利用状況

 |ローカルファイル名|配布元URL| 注記 |
 |-|-|
 | `pakutaso-0001.jpg`| `http://www.pakutaso.com/20150309072post-5277.html` | Sサイズを縮小して利用|
 | `pakutaso-0002.jpg`| `http://www.pakutaso.com/20141015279post-4659.html` | Sサイズを縮小して利用|


## 商用無料の写真検索さん
[![商用無料の写真検索さん](https://nairegift.sakura.ne.jp/freephoto/logo.png){: style="width:35%"}](https://www.nairegift.com/freephoto/)

### 利用条件のメモ
基本的にflickrに登録されている写真のディレクトリサービスなので、実質的にはflickrと同じ。画像毎にライセンスが異なる。

### 利用状況

 |ファイル名|配布元| Copyright | 注記 |
 |-|-|-|
 | `flickr-0001.jpg`| [こちら](https://www.flickr.com/photos/nasacommons/16478237766/) | Public domain [[NASA on the Commons](http://www.nasa.gov/audience/formedia/features/MP_Photo_Guidelines.html#.VRUrXjSsXdI)]| 14人いるが13人しか認識されない|
 | `flickr-0002.jpg`| [こちら](https://www.flickr.com/photos/nasacommons/15871993874/in/set-72157650350042977) | Public domain [[NASA on the Commons](http://www.nasa.gov/audience/formedia/features/MP_Photo_Guidelines.html#.VRUrXjSsXdI)]| |
 | `flickr-0003.jpg`| [こちら](https://www.flickr.com/photos/saipal/2646935127/) | (C) Lap Jaz ([Flickr](https://www.flickr.com/people/saipal/), [Google+](https://www.google.com/+LapJazz)) [[CC 2.0 BY](http://creativecommons.org/licenses/by/2.0/deed.ja)] | |
 | `flickr-0004.jpg`| [こちら](https://www.flickr.com/photos/saipal/270076804/) | (C) Lap Jaz ([Flickr](https://www.flickr.com/people/saipal/), [Google+](https://www.google.com/+LapJazz)) [[CC 2.0 BY](http://creativecommons.org/licenses/by/2.0/deed.ja)]| |

## WIKIMEDIA COMMONS
[![WIKIMEDIA COMMONS](http://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Commons-logo-en.png/90px-Commons-logo-en.png)](http://commons.wikimedia.org/wiki/Main_Page)

### 利用条件のメモ
画像毎にライセンスが異なる。

### 利用状況

#### 外部参照

 |参照先URL|配布元| Copyright | 注記 |
 |-|-|-|
 | [`wikimedia-0001`](http://upload.wikimedia.org/wikipedia/commons/thumb/7/75/SCM9062.jpg/800px-SCM9062.jpg)| [こちら](http://commons.wikimedia.org/wiki/File:SCM9062.jpg?uselang=ja) | (C) Bushra Jabeen [[CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/deed.ja)]| |
 | [`wikimedia-0002`](https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/SCM9062.jpg/800px-SCM9062.jpg)| [こちら](https://commons.wikimedia.org/wiki/File:SCM9062.jpg?uselang=ja) | (C) Bushra Jabeen [[CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/deed.ja)]| `wikimedia-0001`をHTTPSにしただけです |

## pixabay
[![pixabay](http://pixabay.com/static/img/logo_640.png){: style="width:25%"}](http://pixabay.com/ja/)

### 利用条件のメモ
すべてパブリックドメイン

### 利用状況

#### ローカルファイル

 |ファイル名|配布元URL| 注記 |
 |-|-|-|
 | `pixabay-0001.jpg` |[こちら](http://pixabay.com/ja/%E5%A5%B3%E3%81%AE%E5%AD%90-%E7%AC%91%E9%A1%94-%E9%A1%94-%E7%AC%91%E3%81%BF%E3%82%92%E6%B5%AE%E3%81%8B%E3%81%B9%E3%81%A6%E3%81%8F%E3%81%A0%E3%81%95%E3%81%84-%E5%B9%B8%E3%81%9B-%E8%82%96%E5%83%8F%E7%94%BB-%E4%BA%BA%E3%80%85-568205/)|アングロサクソン,子供|
 | `pixabay-0002.jpg` |[こちら](http://pixabay.com/ja/%E3%83%93%E3%83%BC%E3%83%81-%E3%82%AB%E3%83%83%E3%83%97%E3%83%AB-%E5%AE%B6%E6%97%8F-%E6%84%9B-%E5%95%86%E6%A5%AD%E5%86%99%E7%9C%9F%E5%AE%B6-685786/)| アングロサクソン,中年夫婦|
 | `pixabay-0003.jpg` |[こちら](http://pixabay.com/ja/%E5%AE%B6%E6%97%8F-%E3%83%9E%E3%83%9E%E3%81%A8%E8%B5%A4%E3%81%A1%E3%82%83%E3%82%93-%E5%A5%B3%E6%80%A7-%E5%A5%B3%E3%81%AE%E8%B5%A4%E3%81%A1%E3%82%83%E3%82%93-%E6%A5%BD%E3%81%97%E3%81%84-%E5%96%9C%E3%81%B3-692752/)| アジア系,親子|
 | `pixabay-0004.jpg` |[こちら](http://pixabay.com/ja/%E5%AE%B6%E6%97%8F-%E6%B7%B7%E9%9B%91-%E5%AE%B6%E6%97%8F%E3%81%AE%E5%AE%B6-%E5%A4%96-%E9%A6%AC-%E7%BE%8E%E3%81%97%E3%81%84-%E6%98%A5-%E8%8A%B1-255009/)| アジア系,5人|
 | `pixabay-0005.jpg` |[こちら](http://pixabay.com/ja/%E6%AD%A9%E8%A1%8C%E8%80%85%E3%82%BE%E3%83%BC%E3%83%B3-%E4%BA%BA%E9%96%93-%E3%82%B6%E3%83%AB%E3%83%84%E3%83%96%E3%83%AB%E3%82%AF-%E4%BA%BA%E5%8F%A3-%E7%BE%A4%E8%A1%86-552722/)| ヨーロッパ系,雑踏|
 | `pixabay-0006.jpg` |[こちら](http://pixabay.com/ja/%E5%A5%B3%E3%81%AE%E5%AD%90-%E5%A5%B3%E6%80%A7-%E7%92%B0%E5%A2%83%E4%BF%9D%E8%AD%B7-%E6%B0%97%E5%80%99-wwf-%E3%82%A2%E3%83%BC%E3%82%B9%E3%82%A2%E3%83%AF%E3%83%BC-2014-659180/)| アジア系,少女 |
 | `pixabay-0007.jpg` |[こちら](http://pixabay.com/ja/%E5%A5%B3%E3%81%AE%E5%AD%90-%E3%83%92%E3%82%B8%E3%83%A3%E3%83%BC%E3%83%96-%E7%AC%91%E9%A1%94-%E5%A5%B3%E6%80%A7-%E3%82%A4%E3%82%B9%E3%83%A9%E3%83%A0%E6%95%99-%E4%BA%BA-potrait-247302/)| アジア系,少女 |
 | `pixabay-0007.jpg` |[こちら](http://pixabay.com/ja/%E5%A5%B3%E3%81%AE%E5%AD%90-%E3%83%92%E3%82%B8%E3%83%A3%E3%83%BC%E3%83%96-%E7%AC%91%E9%A1%94-%E5%A5%B3%E6%80%A7-%E3%82%A4%E3%82%B9%E3%83%A9%E3%83%A0%E6%95%99-%E4%BA%BA-potrait-247302/)| アジア系,女性 |
 | `pixabay-0008.jpg` |[こちら](http://pixabay.com/ja/%E7%94%B7-%E8%82%96%E5%83%8F%E7%94%BB-%E3%83%93%E3%82%B8%E3%83%8D%E3%82%B9-%E4%B8%AD%E5%9B%BD%E8%AA%9E-%E3%82%A2%E3%82%B8%E3%82%A2-%E7%94%B7%E6%80%A7-%E4%BA%BA-%E3%83%A4%E3%83%B3%E3%82%B0-537136/)| アジア系,男性 |
 | `pixabay-0009.jpg` |[こちら](http://pixabay.com/ja/%E7%94%B7-%E3%82%AD%E3%83%A3%E3%83%83%E3%83%97-%E4%BA%BA-%E5%B8%BD%E5%AD%90-%E8%A1%A3%E6%96%99%E5%93%81-%E8%82%96%E5%83%8F%E7%94%BB-%E9%A1%94-56087/)| ヨーロッパ系,男性 |
 | `pixabay-0005.jpg` |[こちら]()| |

#### 外部参照

 |参照先URL|配布元| 注記 |
 |-|-|-|
 | [`pixabay-0001`](http://pixabay.com/static/uploads/photo/2014/12/14/19/42/girl-568205_640.jpg)| [`http://pixabay.com/ja/...`](http://pixabay.com/ja/%E5%A5%B3%E3%81%AE%E5%AD%90-%E7%AC%91%E9%A1%94-%E9%A1%94-%E7%AC%91%E3%81%BF%E3%82%92%E6%B5%AE%E3%81%8B%E3%81%B9%E3%81%A6%E3%81%8F%E3%81%A0%E3%81%95%E3%81%84-%E5%B9%B8%E3%81%9B-%E8%82%96%E5%83%8F%E7%94%BB-%E4%BA%BA%E3%80%85-568205/)| |

## PublicDomainPictures.net
[PublicDomainPictures.net](http://www.publicdomainpictures.net/browse-author.php?a=1)

### 利用条件のメモ
すべてパブリックドメイン

#### ローカルファイル

 |ファイル名|配布元| 注記 |
 |-|-|-|
 | `pdpicture-0001.jpg` |[こちら](http://www.publicdomainpictures.net/view-image.php?image=10550&picture=3&large=1)||
 | `pdpicture-0002.jpg` |[こちら](http://www.publicdomainpictures.net/view-image.php?image=4319&picture=laptop&large=1)| |
 | `pdpicture-0003.jpg` |[こちら](http://www.publicdomainpictures.net/view-image.php?image=4856&picture=&large=1)| `pdpicture-002`と同一人物 |
 | `pdpicture-0004.jpg` |[こちら](http://www.publicdomainpictures.net/view-image.php?image=4856&picture=&large=1)| 〃 |
 | `pdpicture-0005.jpg` |[こちら](http://www.publicdomainpictures.net/view-image.php?image=3816&picture=card)| 〃 |
 | `pdpicture-0000.jpg` |[``]()||

#### 外部参照

 |参照先URL|配布元| 注記 |
 |-|-|-|
 | [`pdpicture-0006`](http://www.publicdomainpictures.net/pictures/20000/nahled/father-with-daughter-871295107982l35.jpg)| [こちら](http://www.publicdomainpictures.net/view-image.php?image=11285&picture=)| pdpicture-0001の父と娘 |
 | [`pdpicture-0007`](http://www.publicdomainpictures.net/pictures/10000/nahled/1-1258209821ZNbk.jpg)| [こちら](http://www.publicdomainpictures.net/view-image.php?image=4667&picture=laptop)| `pdpicture-0002` と同一人物 |
 | [`pdpicture-0008`](http://www.publicdomainpictures.net/pictures/10000/nahled/1-1256823154Jegu.jpg)| [こちら](http://www.publicdomainpictures.net/view-image.php?image=4542&picture=devil)| 〃 |
 | [`pdpicture-0009`](http://www.publicdomainpictures.net/pictures/10000/nahled/1-1252345800T0DB.jpg)| [こちら](http://www.publicdomainpictures.net/view-image.php?image=3999&picture=devil)| 〃 |
 | [`pdpicture-0010`](http://www.publicdomainpictures.net/pictures/10000/nahled/1-12559753342P9q.jpg)| [こちら](http://www.publicdomainpictures.net/view-image.php?image=4435&picture=wind)| 〃 |
 | [`pdpicture-0011`](http://www.publicdomainpictures.net/pictures/10000/nahled/1-1251822901hnb4.jpg)| [こちら](http://www.publicdomainpictures.net/view-image.php?image=3909&picture=fan)| 〃 |
 | [`pdpicture-0012`](http://www.publicdomainpictures.net/pictures/10000/nahled/1-1251904084vZF6.jpg)| [こちら](http://www.publicdomainpictures.net/view-image.php?image=3930&picture=money)| 〃 |
 | [`pdpicture-0013`](http://www.publicdomainpictures.net/pictures/10000/nahled/1-1255621189mTnS.jpg)| [こちら](http://www.publicdomainpictures.net/view-image.php?image=4403&picture=woman)| 〃 |
 | [`pdpicture-0014`](http://www.publicdomainpictures.net/pictures/30000/nahled/mom-and-child-in-autumn.jpg)| [こちら](http://www.publicdomainpictures.net/view-image.php?image=26173&picture=)| 白人, 母娘|
 | [`pdpicture-0000`]()| [こちら]()||

## PUBLIC-DOMAIN-PHOTOS
[![PUBLIC-DOMAIN-PHOTOS](http://public-domain-photos.com/img/public-domain-photos-and-cliparts.gif){: style="width:35%"}](http://public-domain-photos.com)

### 利用条件のメモ
すべてパブリックドメイン

#### ローカルファイル

 |ファイル名|配布元| 注記 |
 |-|-|-|
 | `pdphotos-0001.jpg` | | |

#### 外部参照

 |参照先URL|配布元| 注記 |
 |-|-|-|
 | [`pdphotos-0001`](http://public-domain-photos.com/free-stock-photos-4/people/face-10.jpg)| [こちら](http://public-domain-photos.com/people/face-10-4.htm)| 白人男性,ここから`pdphotos-0015`まで同一人物 |
 | [`pdphotos-0002`](http://public-domain-photos.com/free-stock-photos-4/people/face-11.jpg)| [こちら](http://public-domain-photos.com/people/face-11-4.htm)| 〃 |
 | [`pdphotos-0003`](http://public-domain-photos.com/free-stock-photos-4/people/face-13.jpg)| [こちら](http://public-domain-photos.com/people/face-13-4.htm)| 〃 |
 | [`pdphotos-0004`](http://public-domain-photos.com/free-stock-photos-4/people/face-14.jpg)| [こちら](http://public-domain-photos.com/people/face-14-4.htm)| 〃 |
 | [`pdphotos-0005`](http://public-domain-photos.com/free-stock-photos-4/people/face-15.jpg)| [こちら](http://public-domain-photos.com/people/face-15-4.htm)| 〃 |
 | [`pdphotos-0006`](http://public-domain-photos.com/free-stock-photos-4/people/face-16.jpg)| [こちら](http://public-domain-photos.com/people/face-16-4.htm)| 〃 |
 | [`pdphotos-0007`](http://public-domain-photos.com/free-stock-photos-4/people/face-18.jpg)| [こちら](http://public-domain-photos.com/people/face-18-4.htm)| 〃 |
 | [`pdphotos-0008`](http://public-domain-photos.com/free-stock-photos-4/people/face-2.jpg)| [こちら](http://public-domain-photos.com/people/face-2-free-stock-photo-4.htm)| 〃 |
 | [`pdphotos-0009`](http://public-domain-photos.com/free-stock-photos-4/people/face-21.jpg)| [こちら](http://public-domain-photos.com/people/face-21-free-stock-photo-4.htm)| 〃 |
 | [`pdphotos-0010`](http://public-domain-photos.com/free-stock-photos-4/people/face-25.jpg)| [こちら](http://public-domain-photos.com/people/face-25-free-stock-photo-4.htm)| 〃 |
 | [`pdphotos-0011`](http://public-domain-photos.com/free-stock-photos-4/people/face-26.jpg)| [こちら](http://public-domain-photos.com/people/face-26-free-stock-photo-4.htm)| 〃 |
 | [`pdphotos-0012`](http://public-domain-photos.com/free-stock-photos-4/people/face-28.jpg)| [こちら](http://public-domain-photos.com/people/face-28-free-stock-photo-4.htm)| 〃 |
 | [`pdphotos-0013`](http://public-domain-photos.com/free-stock-photos-4/people/face-31.jpg)| [こちら](http://public-domain-photos.com/people/face-31-free-stock-photo-4.htm)| 〃 |
 | [`pdphotos-0014`](http://public-domain-photos.com/free-stock-photos-4/people/face-5.jpg)| [こちら](http://public-domain-photos.com/people/face-5-free-stock-photo-4.htm)| 〃 |
 | [`pdphotos-0015`](http://public-domain-photos.com/free-stock-photos-4/people/face-8.jpg)| [こちら](http://public-domain-photos.com/people/face-8-free-stock-photo-4.htm)| 〃 |
 | [`pdphotos-0005`]()| [``]()| |


<!-- vi:set ts=2 sw=2 et: -->

