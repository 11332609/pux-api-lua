{
  {
    accuracy=0.339,
    attribute={
      ANGLE={
        pitch=-12,
        roll=-8,
        yaw=21
      }
    },
    bottom=389,
    left=248,
    left_eye={
      x=311,
      y=223
    },
    right=494,
    right_eye={
      x=408,
      y=208
    },
    top=143
  }
}