{
  {
    accuracy=0.394,
    attribute={
      ANGLE={
        pitch=-10,
        roll=3,
        yaw=-3
      }
    },
    bottom=873,
    left=434,
    left_eye={
      x=554,
      y=548
    },
    right=900,
    right_eye={
      x=754,
      y=555
    },
    top=407
  }
}