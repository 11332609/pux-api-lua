#! /usr/bin/env lua

--[[

  PUX API Interface library for Lua test suites
    
    Copyright (C) 2015 PUX Corporation. All rights reserved.

--]]

_ENV = lunit.module( "webapi_register_normal", "seeall")

local faceu   = require 'pux.webapi.faceu'
local content = require 'pux.webapi.content'
local point   = require 'pux.point'

function setup()
  tc:enter(_NAME)
  -- tc:set_mode( "LOG-CORRECT")

  if not api then
    api = faceu.new {
      api_key = tc.api_key
    }

    api:unregister()
  end
end

function teardown()
  tc:leave()
end

--
-- ローカルファイルからの登録
--
function test_register_from_file()
  src = content.file( tc:path( "flickr-0004.jpg"))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "flickr-0004:md=r", resp)
  assert_true( tc:frame_check("flickr-0004:md=r", resp), "data frame error")
  assert_true( tc:check("flickr-0004:md=r", resp, {ignore = "face_id"}))
end

--
-- メモリデータからの登録
--
function test_register_from_blob()
  src = content.blob( tc:read( "flickr-0004.jpg"), "image/jpeg")

  assert_pass( function() resp = api:register( src) end)
  assert_true( tc:frame_check( "flickr-0004:md=r", resp), "data frame error")
  assert_true( tc:check( "flickr-0004:md=r", resp, {ignore = "face_id"}))
end

--
-- 外部リソースからの登録(HTTP)
--
function test_register_from_url()
  src = content.url( tc:url( 'wikimedia-0001'))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "wikimedia-0001:md=r", resp)
  assert_true( tc:frame_check( "wikimedia-0001:md=r", resp), "data frame error")
  assert_true( tc:check( "wikimedia-0001:md=r", resp, {ignore = "face_id"}))
end

--
-- 外部リソースからの登録(HTTPS)
--
function test_register_from_url_with_https()
  src = content.url( tc:url( 'wikimedia-0002'))

  assert_pass( function() resp = api:register( src) end)
  tc:output( "wikimedia-0002:md=r", resp)
  assert_true( tc:frame_check( "wikimedia-0002:md=r", resp), "data frame error")
  assert_true( tc:check( "wikimedia-0002:md=r", resp, {ignore = "face_id"}))
end

-- vi:set ts=2 sw=2 et:
