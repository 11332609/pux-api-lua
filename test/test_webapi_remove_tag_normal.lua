#! /usr/bin/env lua

--[[

  PUX API Interface library for Lua test suites
    
    Copyright (C) 2015 PUX Corporation. All rights reserved.

--]]

_ENV = lunit.module( "webapi_remove_tag_normal", "seeall")

local faceu   = require 'pux.webapi.faceu'
local content = require 'pux.webapi.content'
local point   = require 'pux.point'

local api
local src
local resp

function setup()
  tc:enter(_NAME)
  -- tc:set_mode( "LOG-CORRECT")

  if not api then
    api = faceu.new {
      api_key = tc.api_key
    }

    api:unregister()

    -- faceId:1
    tags = {
      group = "A",
      klass = "Z",
      name  = "pic-001",
      "test1"
    }
    api:register( content.file( tc:path( "pixabay-0001.jpg")), tags)

    -- faceId:2
    tags = {
      group = "A",
      klass = "Y",
      name  = "pic-002",
      "test1",
      "test2"
    }
    api:register( content.file( tc:path( "pixabay-0001.jpg")), tags)

    -- faceId:3
    tags = {
      group = "B",
      klass = "Y",
      name  = "pic-003",
      "test1",
      "test2"
    }
    api:register( content.file( tc:path( "pixabay-0001.jpg")), tags)

    -- faceId:4
    tags = {
      group = "C",
      klass = "Y",
      name  = "pic-004",
      "test2"
    }
    api:register( content.file( tc:path( "pixabay-0001.jpg")), tags)

    -- faceId:5
    tags = {
      group = "C",
      klass = "Z",
      name  = "pic-005",
      "test3"
    }
    api:register( content.file( tc:path( "pixabay-0001.jpg")), tags)

    -- faceId:6
    tags = {
      group = "D",
      klass = "Z",
      name  = "pic-006",
      "test3"
    }
    api:register( content.file( tc:path( "pixabay-0001.jpg")), tags)

    -- faceId:7 & faceId:8
    tags = {
      group = "D",
      klass = "Z",
      name  = "pic-007",
      "test3"
    }
    api:register( content.url( tc:url( "pdpicture-0014")), tags)
  end
end

function teardown()
  tc:leave()
end

--
-- すべての登録情報から、指定のタグを削除
--
function test_webapi_remove_tags_all()
  assert_pass( function() api:remove_tag( nil, { "group", "klass"}) end)

  resp = api:list()

  tc:output( "remove_all", resp)
  assert( tc:check( "remove_all", resp, {ignore = "registered_image"}))
end

-- vi:set ts=2 sw=2 et:
