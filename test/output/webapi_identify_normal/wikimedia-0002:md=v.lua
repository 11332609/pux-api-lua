{
  {
    accuracy=0.396,
    attribute={},
    bottom=375,
    candidates={
      {
        accuracy=0.99,
        face_id=2,
        tags={}
      }
    },
    face_id=2,
    i12n_accuracy=0.99,
    left=260,
    left_eye={
      x=311,
      y=224
    },
    right=485,
    right_eye={
      x=408,
      y=208
    },
    tags={},
    top=149
  }
}