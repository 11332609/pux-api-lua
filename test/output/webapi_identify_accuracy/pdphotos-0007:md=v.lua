{
  {
    accuracy=0.499,
    attribute={},
    bottom=642,
    candidates={
      {
        accuracy=0.91,
        face_id=2,
        tags={
          guy1=true
        }
      }
    },
    face_id=2,
    i12n_accuracy=0.91,
    left=168,
    left_eye={
      x=276,
      y=354
    },
    right=609,
    right_eye={
      x=464,
      y=357
    },
    tags={
      guy1=true
    },
    top=201
  }
}