#! /usr/bin/env lua

--[[

  PUX API Interface library for Lua test suites
    
    Copyright (C) 2015 PUX Corporation. All rights reserved.

--]]

_ENV = lunit.module( "webapi_register_with_tagging", "seeall")

local faceu   = require 'pux.webapi.faceu'
local content = require 'pux.webapi.content'
local point   = require 'pux.point'

local api
local src
local resp

function setup()
  tc:enter(_NAME)
  -- tc:set_mode( "LOG-CORRECT")

  if not api then
    api = faceu.new {
      api_key = tc.api_key
    }

    api:unregister()
  end

  src = content.url( tc:url( 'pixabay-0001'))
end

function teardown()
  tc:leave()
end

--
-- 単純なタグ 
--
function test_register_with_simple_tags()
  local tags

  tags = {
    test                  = "test",
    ["test012.34-222_12"] = "test",
    "key_only1",                    -- 文字列要素はキーだけのタグ
    key_only2             = true    -- 値がtrueの場合もキーだけのタグと解釈
  }

  resp = api:register( src, tags)

  tc:output( "simple_tags", resp)
  assert_true( tc:frame_check( "simple_tags", resp))
  assert_true( tc:check( "simple_tags", resp, {ignore = "face_id"}))
end

--
-- stringを渡して名前だけのタグを一つのみ設定
--
function test_register_with_name_only_tag()
  resp = api:register( src, "test")
  
  tc:output( "name_only_tag", resp)
  assert_true( tc:frame_check( "name_only_tag", resp))
  assert_true( tc:check( "name_only_tag", resp, {ignore = "face_id"}))
end

--
-- 非ASCIIなタグ 
--
function test_register_with_not_ascii_value_tag()
  local tags

  tags = {
    test = "テスト",
  }

  resp = api:register( src, tags)

  tc:output( "not_ascii", resp)
  assert_true( tc:frame_check( "not_ascii", resp))
  assert_true( tc:check( "not_ascii", resp, {ignore = "face_id"}))

  -- 非ASCIIなタグ名は通らない
  tags = {
    ["テスト"] = "テスト",
  }

  assert_error( function() resp = api:register( src, tags) end)
end

--
-- 長いタグ 
--
function test_register_with_long_tag_value()
  local tags

  -- 60文字以内はOK (!= 60バイト)
  tags = {
    name = "寿限無寿限無五劫の擦り切れ海砂利水魚の水行末雲来末風来末"..
           "食う寝る処に住む処藪ら柑子の藪柑子パイポパイポパイポのここで上限" 
  }

  resp = api:register( src, tags)

  tc:output( "long_value", resp)
  assert_true( tc:frame_check( "long_value", resp))
  assert_true( tc:check( "long_value", resp, {ignore = "face_id"}))

  -- 60文字を超える場合は通らない
  tags = {
    name = "寿限無寿限無五劫の擦り切れ海砂利水魚の水行末雲来末風来末"..
           "食う寝る処に住む処藪ら柑子の藪柑子パイポパイポパイポの上限を無視!" 
  }

  assert_error( function() resp = api:register( src, tags) end)
end

--
-- 長いタグ名
--
function test_register_with_long_tag_key()
  local tags

  -- 20文字以内はOK
  tags = {
    internationalization = "yes" 
  }

  resp = api:register( src, tags)

  tc:output( "long_key", resp)
  assert_true( tc:frame_check( "long_key", resp))
  assert_true( tc:check( "long_key", resp, {ignore = "face_id"}))

  -- 20文字を超える場合は通らない
  tags = {
    internationalizationX = "no" 
  }

  assert_error( function() resp = api:register( src, tags) end)
end

--
-- 設定できるタグの数の上限
--
function test_register_with_limit_number_of_tags()
  local tags

  -- 10個以内はOK
  tags = {
    test1  = "yes",
    test2  = "yes",
    test3  = "yes",
    test4  = "yes",
    test5  = "yes",
    test6  = "yes",
    test7  = "yes",
    test8  = "yes",
    test9  = "yes",
    test10 = "yes",
  }

  resp = api:register( src, tags)

  tc:output( "limit_tags", resp)
  assert_true( tc:frame_check( "limit_tags", resp))
  assert_true( tc:check( "limit_tags", resp, {ignore = "face_id"}))

  -- 10個を超える場合は通らない
  tags = {
    test1  = "yes",
    test2  = "yes",
    test3  = "yes",
    test4  = "yes",
    test5  = "yes",
    test6  = "yes",
    test7  = "yes",
    test8  = "yes",
    test9  = "yes",
    test10 = "yes",
    test11 = "no",
  }

  assert_error( function() resp = api:register( src, tags) end)
end

--
-- 複数のターゲットにタグ付け
--
function test_register_tagged_multiple_person()
  local tags

  api.face_size = "SMALL"
  src  = content.file( tc:path("flickr-0001.jpg"))

  tags = {
    test1  = "yes",
    test2  = "yes",
    test3  = "yes",
    test4  = "yes",
    test5  = "yes",
    test6  = "yes",
    test7  = "yes",
    test8  = "yes",
    test9  = "yes",
    test10 = "yes",
  }

  resp = api:register( src, tags)

  tc:output( "multi_person", resp)
  assert_true( tc:frame_check( "multi_person", resp))
  assert_true( tc:check( "multi_person", resp, {ignore = "face_id"}))
end

-- vi:set ts=2 sw=2 et:
