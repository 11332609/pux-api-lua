#! /usr/bin/env lua

--[[

  PUX API Interface library for Lua test suites
    
    Copyright (C) 2015 PUX Corporation. All rights reserved.

--]]

_ENV = lunit.module( "webapi_identify_number_of_person", "seeall")

local faceu   = require 'pux.webapi.faceu'
local content = require 'pux.webapi.content'
local point   = require 'pux.point'

local api
local src
local resp

function setup()
  tc:enter(_NAME)
  -- tc:set_mode( "LOG-CORRECT")

  if not api then
    api = faceu.new {
      api_key   = tc.api_key,
      face_size = "SMALL"
    }

    api:unregister()

    api:register( content.file( tc:path( "pixabay-0004.jpg")), {picture = "1"})
    api:register( content.file( tc:path( "pixabay-0005.jpg")), {picture = "2"})
    api:register( content.file( tc:path( "flickr-0001.jpg")), {picture = "3"})
    api:register( content.url( tc:url( "pdpicture-0006")), {picture = "4"})
  end
end

function teardown()
  tc:leave()
end

--
-- 0人
--
function test_identify_no_persons()
  local i
  local info

  src  = content.file( tc:path( "pixabay-0005.jpg"))
  resp = api:who( src)

  tc:output( "pixabay-0005:md:=v:fs=s", resp)
  assert_equal( 0, #resp)

  for i,info in ipairs(resp) do
    assert( info.candidate[1].accuracy > 0.8)
    assert_equal( '2', info.tags.picture)
  end
end

--
-- 6人
--
function test_identify_6_persons()
  local i
  local info

  src  = content.file( tc:path( "pixabay-0004.jpg"))
  resp = api:who( src)

  tc:output( "pixabay-0004:md=v:fs=s", resp)
  assert_equal( 6, #resp)

  for i,info in ipairs(resp) do
    assert( info.candidates[1].accuracy > 0.8)
    assert_equal( '1', info.tags.picture)
  end
end

--
-- 13人
--
function test_identify_13_persons()
  local i
  local info

  src  = content.file( tc:path( "flickr-0001.jpg"))
  resp = api:who( src)

  tc:output( "flickr-0001:md=v:fs=s", resp)
  assert_equal( 13, #resp)

  for i,info in ipairs(resp) do
    assert( info.candidates[1].accuracy > 0.8)
    assert_equal( '3', info.tags.picture)
  end
end

--
-- 三人中二人
--
function test_identify_2_on_3_persons()
  local i
  local info

  src  = content.file( tc:path( "pdpicture-0001.jpg"))
  resp = api:who( src)

  tc:output( "pdpicture-0001:md=v:fs=s", resp)
  assert_equal( 2, #resp)

  for i,info in ipairs(resp) do
    assert( info.candidates[1].accuracy > 0.8)
    assert_equal( '4', info.tags.picture)
  end
end



-- vi:set ts=2 sw=2 et:
