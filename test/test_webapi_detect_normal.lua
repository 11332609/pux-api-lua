#! /usr/bin/env lua

--[[

  PUX API Interface library for Lua test suites
    
    Copyright (C) 2015 PUX Corporation. All rights reserved.

--]]

_ENV = lunit.module( "webapi_detect_normal", "seeall")

local faceu   = require 'pux.webapi.faceu'
local content = require 'pux.webapi.content'
local point   = require 'pux.point'


local api
local src
local resp

function setup()
  tc:enter(_NAME)

  api = faceu.new {
    api_key = tc.api_key
  }
end

function teardown()
  tc:leave()
end

--
-- ローカルファイルからの検出
--
function test_detect_from_file()
  src = content.file( tc:path( "flickr-0004.jpg"))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "flickr-0004", resp)
  assert_true( tc:frame_check("flickr-0004", resp), "frame check failed")
  assert_true( tc:check("flickr-0004", resp))
end

--
-- メモリデータからの検出
--
function test_detect_from_blob()
  src = content.blob( tc:read( "flickr-0004.jpg"), "image/jpeg")

  assert_pass( function() resp = api:detect( src) end)
  assert_true( tc:frame_check( "flickr-0004", resp), "frame check failed")
  assert_true( tc:check( "flickr-0004", resp))
end

--
-- 外部リソースからの検出(HTTP)
--
function test_detect_from_url()
  src = content.url( tc:url('wikimedia-0001'))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "wikimedia-0001", resp)
  assert_true( tc:frame_check( "wikimedia-0001", resp), "frame check failed")
  assert_true( tc:check( "wikimedia-0001", resp))
end

--
-- 外部リソースからの検出(HTTPS)
--
function test_detect_from_url_with_https()
  src = content.url( tc:url('wikimedia-0002'))

  assert_pass( function() resp = api:detect( src) end)
  tc:output( "wikimedia-0002", resp)
  assert_true( tc:frame_check( "wikimedia-0002", resp), "frame check failed")
  assert_true( tc:check( "wikimedia-0002", resp))
end

-- vi:set ts=2 sw=2 et:
