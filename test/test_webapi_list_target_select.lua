#! /usr/bin/env lua

--[[

  PUX API Interface library for Lua test suites
    
    Copyright (C) 2015 PUX Corporation. All rights reserved.

--]]

_ENV = lunit.module( "webapi_list_target_select", "seeall")

local faceu   = require 'pux.webapi.faceu'
local content = require 'pux.webapi.content'
local point   = require 'pux.point'

local api
local src
local resp
local cond

local opt = {
  ignore = "registered_image"
}

function setup()
  tc:enter(_NAME)
  -- tc:set_mode( "LOG-CORRECT")

  if not api then
    api = faceu.new {
      api_key = tc.api_key
    }

    api:unregister()

    -- faceId:1
    tags = {
      group = "A",
      klass = "Z",
      name  = "pic-001",
      "test1"
    }
    api:register( content.file( tc:path( "pixabay-0001.jpg")), tags)

    -- faceId:2
    tags = {
      group = "A",
      klass = "Y",
      name  = "pic-002",
      "test1",
      "test2"
    }
    api:register( content.file( tc:path( "pixabay-0001.jpg")), tags)

    -- faceId:3
    tags = {
      group = "B",
      klass = "Y",
      name  = "pic-003",
      "test1",
      "test2"
    }
    api:register( content.file( tc:path( "pixabay-0001.jpg")), tags)

    -- faceId:4
    tags = {
      group = "C",
      klass = "Y",
      name  = "pic-004",
      "test2"
    }
    api:register( content.file( tc:path( "pixabay-0001.jpg")), tags)

    -- faceId:5
    tags = {
      group = "C",
      klass = "Z",
      name  = "pic-005",
      "test3"
    }
    api:register( content.file( tc:path( "pixabay-0001.jpg")), tags)

    -- faceId:6
    tags = {
      group = "D",
      klass = "Z",
      name  = "pic-006",
      "test1",
      "test3"
    }
    api:register( content.file( tc:path( "pixabay-0001.jpg")), tags)

    -- faceId:7 & faceId:8
    tags = {
      group = "D",
      klass = "Z",
      name  = "pic-007",
      "test1",
      "test3"
    }
    api:register( content.url( tc:url( "pdpicture-0014")), tags)
  end
end

function teardown()
  tc:leave()
end

--
-- タグが含まれているかどうかで選択
--
function test_list_select_by_tag_includes()
  cond = "test1"

  resp = api:list( cond)

  tc:output( "select_by_tag_includes", resp)
  assert_equal( 6, #resp)
  assert( "select_by_tag_includes", resp, opt)
end

--
--  複数のタグが含まれているかどうか(AND条件となる)
--
function test_webapi_list_select_by_multi_tag_includes()
  resp = api:list {
    "test1",
    "test2",
  }

  tc:output( "select_by_multi_tag_includes", resp)
  assert_equal( 2, #resp)
  assert( tc:check( "select_by_multi_tag_includes", resp, opt))
end

--
-- タグの値で選択
--
function test_webapi_list_select_by_tag_value()
  resp = api:list {
    group = "A"
  }

  tc:output( "select_by_tag_value", resp)
  assert_equal( 2, #resp)
  assert( tc:check( "select_by_tag_value", resp, opt))
end

--
--  キーの異なる複数のタグの値で選択(AND条件となる)
--
function test_webapi_list_select_by_multi_tag_value()
  resp = api:list {
    group = "A",
    klass = "Z"
  }

  tc:output( "select_by_multi_tags_value", resp)
  assert_equal( 1, #resp)
  assert( tc:check( "select_by_multi_tags_value", resp, opt))
end

--
-- キーが同じ複数のタグの値で選択(OR条件となる)
--
function test_webapi_list_select_by_multi_tags_of_same_key()
  resp = api:list {
    group = { "A", "B", "D"}
  }

  tc:output( "select_by_multi_tags_of_same_key", resp)
  assert_equal( 6, #resp)
  assert( tc:check( "select_by_multi_tags_of_same_key", resp, opt))
end

--
-- faceIdで選択
--
function test_webapi_list_select_by_face_id()
  resp = api:list( 3)

  tc:output( "select_by_face_id", resp)
  assert_equal( 1, #resp)
  assert( tc:check( "select_by_face_id", resp, opt))
end

--
-- 複数のfaceIdで選択(OR条件になる)
--
function test_webapi_list_select_by_multi_face_id()
  resp = api:list{ 1, 4, 8 }

  tc:output( "select_by_multi_face_id", resp)
  assert_equal( 3, #resp)
  assert( tc:check( "select_by_multi_face_id", resp, opt))
end

--
-- 複合条件
--
function test_webapi_list_select_by_complex_condition_1()
  resp = api:list {
    klass = "Y",
    "test2"
  }

  tc:output( "select_by_complex_condition_1", resp)
  assert_equal( 3, #resp)
  assert( tc:check( "select_by_complex_condition_1", resp, opt))
end

function test_webapi_list_select_by_complex_condition_2()
  resp = api:list {
    group = { "B", "C" },
    klass = "Y"
  }

  tc:output( "select_by_complex_condition_2", resp)
  assert_equal( 2, #resp)
  assert( tc:check( "select_by_complex_condition_2", resp, opt))
end

function test_webapi_list_select_by_complex_condition_3()
  resp = api:list {
    group = { "B", "C" },
    klass = "Y",
    "test1"
  }

  tc:output( "select_by_complex_condition_3", resp)
  assert_equal( 1, #resp)
  assert( tc:check( "select_by_complex_condition_3", resp, opt))
end

-- vi:set ts=2 sw=2 et:
