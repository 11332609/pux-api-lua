# PUX API interface library for Lua

T.B.D  
Reference => [here](doc/index.html)

## 使い方
以下のライブラリを使用していますので、事前にインストールしておいてください(いずれもluarocksでインストール可能です)。

 * LuaSocket 3.0rc1
 * Lua CJSON 2.1.0
 * lunitx-0.8 (テストを使用する場合)
 * luaposix-33.3.1 (テストを使用する場合)
 * LDoc 1.4.3 (ドキュメント生成を行う場合)

あとはlib/puxを適当なところにコピーして環境変数LUA_PATHの調整を行ってください。
だれかluarocks化してください。

## テストの実行方法

test/config.luaにAPIキーを設定して、トップディレクトリでrun_test.shを実行してください。
また、以下の様に実行することで特定のテスト関数のみ実行することが可能です。

    ./run_test.sh -t test_detect_from_file

上記の実行例では、test/test_detect_normal.luaで定義されているtest_detect_from_file()が実行されます。

## サンプル 

### 顔検出
    faceu   = require 'pux.webapi.faceu'
    content = require 'pux.webapi.content'
    
    api  = faceu.new{ api_key = 'XXXXXXXXXXX', age > true }
    resp = api:detect( content.file( "sample.jpg"))

### 顔識別

#### 登録
    rep = api:register( content.new( "sample1.jpg"),
			{ name = '田中一郎', group = "team-A" })

#### 識別
    resp = api:who( content.file( "sample2.jpg"), { group = "team-A"new})

#### 登録解除
    resp = api:unregister{ name = '田中一郎' }

## TODO
そのうちヒマができたら

* ドキュメント整備
* API追加(WebAPIオブジェクト認識, ネイティブSDK対応)
* サンプル追加
* rock化
* Unitテストの整備 （作業中）
