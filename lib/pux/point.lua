#! /usr/bin/lua

--[[

 PUX API interface library for Lua

    Copyright (C) 2015 PUX Corporation. All rights reserved.

--]]

---
-- PUX APIが使用する"点"を抽象化したクラスです。
--
-- @classmod pux.Point
--
local Point = {}

Point.__index = Point

---
-- @lfunction __eq
--
local function __eq( op1, op2)
  return (type(op1) == type(op2) and op1.x == op2.x and op1.y == op2.y)
end

---
-- @lfunction __tostring
--
local function __tostring( self)
  return string.format( "{x:%d, y:%d}", self.x, self.y)
end

Point.meta = {
  __eq       = __eq,
  __tostring = __tostring
}

---
-- X座標
-- @field x
--

---
-- Y座標
-- @field y
--

---
-- Pointインスタンスの生成
--
-- @function pux.Point.new
--
-- @tparam number x
--   新しく生成するインスタンスのX座標を指定します
--
-- @tparam number y
--   新しく生成するインスタンスのY座標を指定します
--
function Point.new( x, y)
  ret = {
    x = x,
    y = y
  }

  setmetatable( ret, Point.meta)

  return ret
end

---
-- 指定されたテーブルを強制的にpux.Point化します。
--
-- @function pux.Point.force_cast
--
-- @tparam table obj
--   Point化する対象のテーブルを指定します。
--
-- @treturn pux.Point
--   Point化されたテーブルを返します(受け取ったobjを返します)。
--
-- @raise
--   - 渡されたオブジェクトがテーブル以外の場合に例外が発生します
--     (Object is not table)
--
function Point.force_cast( obj)
  if type(obj) ~= "table" then
    error( "Target object is not table.")
  end

  setmetatable( obj, Point.meta)

  return obj
end

return Point

-- vi:set ts=2 sw=2 et:
