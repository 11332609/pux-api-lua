#! /usr/bin/lua

--[[

 PUX API interface library for Lua

    Copyright (C) 2015 PUX Corporation. All rights reserved.

--]]

---
-- PUX API提供される顔検出機能で返される一人分のデータを格納するクラスです。
--
-- @classmod pux.Person
--
local Person = {}

Person.__index = Person

---
-- @lfunction __eq
--
local function __eq( op1, op2)
  local ret

  ret = (type(op1)  == type(op2) and
        (op1.left   == op2.left)  and
        (op1.top    == op2.top)   and
        (op1.right  == op2.right) and
        (op1.bottom == op2.bottom)

  return (type(op1) == type(op2) and op1.x == op2.x and op1.y == op2.y)
end

Person.meta = {
  __eq       = __eq,
}

---
-- 検出確度
-- @field accuracy
--   検出結果の確度を0.0〜1.0の範囲で格納しています。
--

---
-- 顔検出位置(左端)
-- @field left
--   顔として検出した矩形の左端座標を格納しています。
--

---
-- 顔検出位置(上端)
-- @field top
--   顔として検出した矩形の上端座標を格納しています。
--

---
-- 顔検出位置(右端)
-- @field right
--   顔として検出した矩形の右端座標を格納しています。
--

---
-- 顔検出位置(下端)
-- @field bottom
--   顔として検出した矩形の下端座標を格納しています。
--

---
-- 左目検出位置
-- @field left_eye
--   検出した左目の中心座標を@{pux.Point}で格納しています。
--

---
-- 右目検出位置
-- @field right_eye
--   検出した左目の中心座標を@{pux.Point}で格納しています。
--

---
-- 指定されたテーブルを強制的に@{pux.Person}化します。
--
-- @function pux.Person.force_cast
--
-- @tparam table obj
--   Point化する対象のテーブルを指定します。
--
-- @treturn pux.Person
--   Point化されたテーブルを返します(受け取ったobjを返します)。
--
-- @raise
--   - 渡されたオブジェクトがテーブル以外の場合に例外が発生します
--     `(Object is not table)`
--
function Person.force_cast( obj)
  if type(obj) ~= "table" then
    error( "Target object is not table.")
  end

  setmetatable( obj, Person.meta)

  return obj
end

return Person

-- vi:set ts=2 sw=2 et:
