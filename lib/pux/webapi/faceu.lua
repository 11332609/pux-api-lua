#! /usr/bin/lua

--[[

 PUX API interface library for Lua

    Copyright (C) 2015 PUX Corporation. All rights reserved.

--]]

local http  = require 'socket.http'
local ltn12 = require 'ltn12'
local mime  = require 'mime'
local url   = require 'socket.url'
local json  = require 'cjson'
local point = require 'pux.point'

local dbg   = require 'dbg_util'

---
-- PUXの提供するWebAPIのFaceU機能の呼び出しインタフェースクラスです。
-- WebAPIで用意される機能のうち、FaceU関連の機能(顔検出,顔認証)の機能はこの
-- クラスのインスタンスを通して呼び出します.
--
-- @classmod pux.webapi.FaceU
--
-- @usage
--  faceu   = require 'pux.webapi.faceu'
--  content = require 'pux.webapi.content'
--  
--  api = faceu.new {
--      api_key      = 'xxxxxxxxxxxxxxx',
--      funny_option = true
--  } 
--  
--  resp = api:detect( content.file( "sample.jpg"))
--
local FaceU = {
  default_service_url  = "http://eval.api.polestars.jp:8080/webapi",
  default_agent_name   = "PUX WebAPI Interface for Lua/0.1",

  default_face_size    = "MIDDLE",
  default_parts_detect = false,
  default_blink        = false,
  default_age          = false,
  default_gender       = false,
  default_angle        = false,
  default_smile        = false,
  default_funny_option = false,
}

FaceU.__index = FaceU

---
-- エージェント名
-- @field agent_name 
--   リクエスト発行時に`User-Agent`に設定する値を設定します。
--   (`default: "PUX WebAPI Interface for Lua/0.1"`)

---
-- APIキー
-- @field api_key 
--   ユーザ登録時に付与されたAPIキーを設定してください。
--   デフォルト値は`nil`が設定されていますので、必ずAPI呼び出し前に設定してく
--   ださい。
--   (`default: nil`)

---
-- 検出オプション（最小顔サイズ）
-- @field face_size 
--   顔検出を行う場合の最小顔サイズを指定します。`"SMALL"`,`"MIDDLE"`,`"LARGE"`
--   のいずれかを設定してください。
--   (`default: "MIDDLE"`)
--

---
-- 検出オプション（パーツ検出）
-- @field parts_detect 
--   顔検出時に部品検出を行う場合は`true`を設定します。
--   (`default: false`)
--

---
-- 検出オプション（まばたき検出）
-- @field blink 
--   顔検出時にまばたき検出を行う場合は`true`を設定します。
--   (`default: false`)
--

---
-- 検出オプション（年齢検出）
-- @field age 
--   顔検出時に年齢検出を行う場合は`true`を設定します。
--   (`default: false`)
--

---
-- 検出オプション（性別検出）
-- @field gender 
--   顔検出時に性別検出を行う場合は`true`を設定します。
--   (`default: false`)
--

---
-- 検出オプション（顔向き確度検出）
-- @field angle 
--   顔検出時に顔向き確度性別検出を行う場合は`true`を設定します。
--   (`default: false`)
--

---
-- 検出オプション（笑顔判定）
-- @field smile 
--   顔検出時に笑顔判定を行う場合は`true`を設定します。
--   (`default: false`)
--

---
-- 検出オプション（おまけ機能）
-- @field funny_option 
--   顔検出時におまけ機能を使用する場合は`true`を設定します。
--   (`default: false`)
--

---
-- @lfunction build_query
--
local function build_query(args)
  local ret
  local first
  local i
  local val
  local sep

  if args then
    first = true
    ret   = ""
    sep   = "?"

    for i,val in ipairs(args) do
      ret = ret .. string.format( "%s%s=%s", sep, val[1], val[2])

      if first then
        first = false
        sep   = "&"
      end
    end

  else
    ret = ""
  end

  return ret
end

---
-- @lfunction http:get
--
function http:get( url, args, headers)
  local body
  local code
  local header
  local nul

  body = {}

  nul, code, header = self.request {
    method  = "GET",
    url     = url .. build_query( args),
    sink    = ltn12.sink.table( body),
    headers = headers
  }

  return table.concat(body), code, header
end

---
-- @lfunction http:post
--
function http:post( url, args, headers, source)
  local body
  local code
  local header
  local nul

  body = {}

  nul, code, header = self.request {
    method  = "POST",
    url     = url .. build_query( args),
    sink    = ltn12.sink.table( body),
    source  = source,
    headers = headers
  }

  return table.concat(body), code, header
end

---
-- @lfunction table_clone
--
local function table_clone( src)
  local ret
  local key
  local val

  ret = {}

  for key,val in pairs(src) do
    ret[key] = val
  end

  return ret
end

---
-- @lfunction table_pick
--
local function table_pick( src, tag)
  local ret

  ret = src[tag]
  src[tag] = nil

  return ret
end

---
-- @lfunction get_request
--
local function get_request( self, args, header)
  local header
  local key
  local val

  header = header or {}
  header["User-Agent"] = self.agent_name

  return http:get( self.service_url .. "/face.do", args, header)
end

---
-- @lfunction post_request
--
local function post_request( self, args, content, header)
  local header
  local body
  local key
  local val
  local ret

  header = header or {}
  header["User-Agent"] = self.agent_name

  for key,val in pairs(content.header) do
    header[key] = val
  end

  return http:post(self.service_url .. "/face.do", args, header, content.source)
end

---
-- @lfunction inject
--
local function inject( self, args, content)
  local body 
  local code 
  local header 
  local resp

  if not self.api_key or type(self.api_key) ~= "string" then
    error( "valid API key is not set.")
  end

  args[#args+1] = { "apiKey", self.api_key }
  args[#args+1] = { "response", "json" }

  if content then
    body,code,header = post_request( self, args, content, header)
  else
    body,code,header = get_request( self, args, header)
  end

  resp = json.decode( body)

  if code ~= 200 then
    error( resp.results.faceRecognition.errorMessage)
  end

  return resp.results.faceRecognition
end

local face_size_table = {
  SMALL  = 1,
  MIDDLE = 2,
  LARGE  = 3,
  [1]    = 1,
  [2]    = 2,
  [3]    = 3,
}

---
-- @lfunction create_common_arguments
--
local function create_common_arguments( self)
  local ret
  local face_size

  ret = {}

  face_size = face_size_table[ self.face_size]

  if face_size then
    ret[#ret+1] = { "optionFlgMinFaceWidth", face_size }
  end

  if not self.parts_detect then
    ret[#ret+1] = { "facePartsCoordinates", 0 }
  end
  
  if not self.blink then
    ret[#ret+1] = { "blinkJudge", 0 }
  end

  if not self.age and not self.gender then
    ret[#ret+1] = { "ageAndgenderJudge", 0 }
  end

  if not self.angle then
    ret[#ret+1] = { "angleJudge", 0 }
  end

  if not self.smile then
    ret[#ret+1] = { "smileJudge", 0 }
  end

  if self.funny_option then
    ret[#ret+1] = { "enjoyJudge", 1 }
  end

  return ret
end

---
-- @lfunction append_tag_info
--
local function append_tag_info( args, tags)
  local key
  local val
  local data

  if type(tags) == "string" then
    args[#args+1] = { "tag", tags }

  elseif type(tags) == "table" then
    for key,val in pairs(tags) do
      if type(key) == "string" and val == true then
        val = key
        key = 0

      elseif type(val) ~= "string" then
        error( "Illeagal tag value type.")
      end

      if type(key) == "string" then
        data = { "tag",  (key .. ":" .. val) }

      elseif type(key) == "number" then
        data = { "tag", val }

      else
        error( "Illeagal condition info.")
      end

      args[#args+1] = data
    end

  else
    error( "Not supported tags type.")
  end

  return args
end

---
-- @lfunction append_tag_cond
--
local function append_tag_cond( args, tags)
  local key
  local val
  local typ
  local tmp

  if type(tags) == "string" then
    args[#args+1] = { "tagCondition", tags }

  elseif type(tags) == "table" then
    for key,val in pairs(tags) do
      -- 速度的には不利ですが、読みやすくなるので条件をいったん文字列に変換
      -- して、それをキーに処理を選択するようにしています。

      typ = type(key) .. ":" .. type(val)

      if typ == "string:string" then
        args[#args+1] = { "tagCondition", key .. ":" .. val } 

      elseif typ == "string:table" then
        for tmp,val in pairs(val) do
          args[#args+1] = { "tagCondition", key .. ":" .. val } 
        end

      elseif typ == "number:string" then
        args[#args+1] = { "tagCondition", val }

      else
        error( "Illeagal condition info.")
      end
    end

  elseif tags ~= nil then
    error( "Not supported tag type.")
  end
end

---
-- @lfunction append_cond_info
--
local function append_cond_info( args, cond)
  local key
  local val
  local typ
  local tmp

  typ = type( cond)

  if typ == "string" then
    args[#args+1] = { "tagCondition", cond }

  elseif typ == "number" then
    args[#args+1] = { "faceId", string.format( "%d", cond) }

  elseif typ == "table" then
    for key,val in pairs(cond) do
      -- 速度的には不利ですが、読みやすくなるので条件をいったん文字列に変換
      -- して、それをキーに処理を選択するようにしています。

      typ = type(key) .. ":" .. type(val)

      if typ == "string:string" then
        args[#args+1] = { "tagCondition", key .. ":" .. val } 

      elseif typ == "string:table" then
        for tmp,val in pairs(val) do
          args[#args+1] = { "tagCondition", key .. ":" .. val } 
        end

      elseif typ == "number:number" then
        args[#args+1] = { "faceId", string.format( "%d", val) }

      elseif typ == "number:string" then
        args[#args+1] = { "tagCondition", val }

      else
        error( "Illeagal condition info.")
      end
    end

  elseif cond == nil then
    -- Nothing

  else
    error( "Not supported condition type.")
  end

  return args
end

---
-- 顔検出を行った際に返される一人分のデータを格納したテーブルです.
-- @{pux.Person}をベースとしています。
--
-- @table PersonForFaceDetect
--
-- @field accuracy
--   検出結果の確度を0.0〜1.0の範囲で格納しています。
--
-- @field left
--   顔として検出した矩形の左端座標を格納しています。
--
-- @field top
--   顔として検出した矩形の上端座標を格納しています。
--
-- @field right
--   顔として検出した矩形の右端座標を格納しています。
--
-- @field bottom
--   顔として検出した矩形の下端座標を格納しています。
--
-- @field left_eye
--   検出した左目の中心座標を@{pux.Point}で格納しています。
--
-- @field right_eye
--   検出した左目の中心座標を@{pux.Point}で格納しています。
--
-- @field attribute
--   オプション指定で検出された情報が格納されています。
--
-- @field parts
--   パーツ検出有効時に検出結果が格納されます。
-- 

---
-- @lfunction convert_to_detect_response
--
local function convert_to_detect_response( resp)
  local ret
  local info
  local data
  local key
  local val
  local tmp

  info = resp.faceCoordinates 

  ret = {
    accuracy = info.faceConfidence / 1000.0,
    left     = info.faceFrameLeftX,
    top      = info.faceFrameTopY,
    right    = info.faceFrameRightX,
    bottom   = info.faceFrameBottomY,

    left_eye  = {
      x = info.leftEyeX,
      y = info.leftEyeY,
    },

    right_eye = {
      x = info.rightEyeX,
      y = info.rightEyeY,
    },

    attribute = {}
  }

  point.force_cast( ret.left_eye)
  point.force_cast( ret.right_eye)

  if resp.registrationFaceInfo then
    info = resp.registrationFaceInfo

    ret.face_id = info.faceId
    ret.tags    = {}

    if info.meta.tag then
      for i,val in ipairs(info.meta.tag) do
        ret.tags[val.key] = val.value or true
      end
    end
  end

  if resp.facePartsCoordinates then
    data = {}

    for key,val in pairs(resp.facePartsCoordinates ) do
      if type(val) == "table" then
        data[key:gsub("([A-Z])", "_%1"):upper()] = val
        point.force_cast( val)
      end
    end

    ret.parts = data
  end

  if resp.blinkJudge then
    ret.attribute.BLINK = {
      left  = resp.blinkJudge.leftEye / 65535.0,
      right = resp.blinkJudge.rightEye / 65535.0
    }
  end

  if resp.ageJudge then
    ret.attribute.AGE = {
      value    = resp.ageJudge.ageResult,
      accuracy = resp.ageJudge.ageScore / 1000.0
    }
  end

  if resp.genderJudge then
    local gender

    if resp.genderJudge.genderResult == 0 then
      gender = "MALE"

    elseif resp.genderJudge.genderResult == 1 then
      gender = "FEMALE"

    else
      gender = "UNKNOWN"
    end

    ret.attribute.GENDER = {
      value    = gender,
      accuracy = resp.genderJudge.genderScore / 1000.0
    }
  end

  if resp.smileJudge then
    ret.attribute.SMILE = resp.smileJudge.smileLevel / 100.0
  end

  if resp.faceAngleJudge then
    ret.attribute.ANGLE = {
      yaw   = resp.faceAngleJudge.yaw,
      pitch = resp.faceAngleJudge.pitch,
      roll  = resp.faceAngleJudge.roll
    }
  end

  if resp.enjoyJudge then
    ret.attribute.ANIMALIZE = resp.enjoyJudge.similarAnimal
    ret.attribute.SMUG      = resp.enjoyJudge.doyaLevel / 100.0
    ret.attribute.TROUBLED  = resp.enjoyJudge.troubleLevel / 100.0
  end

  return ret
end

---
-- @lfunction parse_face_detect_response
--
local function parse_face_detect_response( resp)
  local ret
  local i
  local result

  ret = {}

  if resp.detectionFaceInfo then
    for i,result in ipairs(resp.detectionFaceInfo) do
      ret[i] = convert_to_detect_response( result)
    end
  end
  
  return ret
end

---
-- 顔識別を行った際に返される一人分のデータを格納したテーブルです。
-- @{pux.Person}をベースとしています。
--
-- @table PersonForFaceIdentify
--
-- @field accuracy
--   検出結果の確度を0.0〜1.0の範囲で格納しています。
--
-- @field left
--   顔として検出した矩形の左端座標を格納しています。
--
-- @field top
--   顔として検出した矩形の上端座標を格納しています。
--
-- @field right
--   顔として検出した矩形の右端座標を格納しています。
--
-- @field bottom
--   顔として検出した矩形の下端座標を格納しています。
--
-- @field left_eye
--   検出した左目の中心座標を@{pux.Point}で格納しています。
--
-- @field right_eye
--   検出した左目の中心座標を@{pux.Point}で格納しています。
--
-- @field attribute
--   オプション指定で検出された情報が格納されています。
--
-- @field candidates
--   認識候補のリストを@{WhoInfo}テーブルのリストで格納しています。
--   このリストは確度順にソートされています。
--
-- @field face_id
--   最も確度の高い識別候補の`face_id`フィールドがコピーされています。
--
-- @field tags
--   最も確度の高い識別候補の`tags`フィールドがコピーされています。
--
-- @field i12n_accuracy
--   最も確度の高い識別候補の`accuracy`フィールドがコピーされています。
--
-- @field registered_image
--   最も確度の高い識別候補の`registered_image`フィールドがコピーされています。
--

---
-- 識別対象を特定する為の情報を格納したテーブルです
--
-- @table WhoInfo
--
-- @field face_id
--   対応するfaceIdが格納されています。
--
-- @field tags
--   タグ情報がtableで格納されています。
-- 
-- @field accuracy
--   識別の確度が0.0〜1.0の範囲で格納されています。
-- 
-- @field registered_image
--   登録されている顔画像を取得する為のURLが@{string}で格納されています。
--

---
-- @lfunction convert_to_who_info
--
local function convert_to_who_info( data)
  local ret
  local i
  local val

  ret = {
    face_id = data.faceId,
    tags    = {}
  }

  if data.meta.tag then
    for i,val in ipairs(data.meta.tag) do
      ret.tags[val.key] = val.value or true
    end
  end

  if data.score then
    ret.accuracy = data.score / 100.0
  end

  if data.imagePath then
    ret.registered_image = data.imagePath
  end

  return ret
end

---
-- @lfunction convert_to_identify_response
--
local function convert_to_identify_response( resp)
  local ret
  local info
  local i
  local val
  local data

  info = resp.faceCoordinates 

  ret = {
    accuracy = info.faceConfidence / 1000.0,
    left     = info.faceFrameLeftX,
    top      = info.faceFrameTopY,
    right    = info.faceFrameRightX,
    bottom   = info.faceFrameBottomY,

    left_eye = {
      x = info.leftEyeX,
      y = info.leftEyeY,
    },

    right_eye = {
      x = info.rightEyeX,
      y = info.rightEyeY,
    },

    attribute  = {},
    candidates = {}
  }

  point.force_cast( ret.left_eye)
  point.force_cast( ret.right_eye)

  for i,val in pairs(resp.candidate) do
    ret.candidates[#ret.candidates+1] = convert_to_who_info( val)
  end

  data                 = ret.candidates[1]
  ret.face_id          = data.face_id
  ret.tags             = data.tags
  ret.i12n_accuracy    = data.accuracy
  ret.registered_image = data.registered_image

  return ret
end

---
-- @lfunction parse_face_identify_response
--
local function parse_face_identify_response( resp)
  local ret
  local i
  local result

  ret = {}

  if resp.verificationFaceInfo then
    for i,result in ipairs(resp.verificationFaceInfo) do
      if result.candidate then
        ret[#ret+1] = convert_to_identify_response( result)
      end
    end
  end
  
  return ret
end

---
-- 顔検出機能の呼び出しを行います.検出対象の画像コンテンツを引数で指定します.
--
-- @function detect
--
-- @tparam pux.webapi.Source content
--   顔検出を行う対象を指定します
--
-- @return
--   検出した顔情報を@{PersonForFaceDetect}のリストで返します。　
--
-- @raise
--   - APIキーを設定せずに本メソッドを呼出場合に例外が発生します。
--     (API key is not set)
--
-- @usage
--   faceu   = require 'pux.webapi.faceu'
--   content = require 'pux.webapi.content'
--
--   api = faceu.new { api_key = "XXXXXXX..." }
--
--   -- ローカルファイルから検出を行う場合
--   api:detect( content.file( "sample.jpg"))
--
--   -- 外部のリソースを指定して検出を行う場合
--   api:detect( content.url( "http://case.of.example/dummy.jpg"))
--
function FaceU.detect( self, content)
  local args
  local resp

  args = create_common_arguments( self)

  if content.type == "URL" then
    args[#args+1] = { "imageURL", url.escape( content.url) }

    content = nil
  end

  resp = inject( self, args, content)

  return parse_face_detect_response( resp)
end

---
-- 認証用顔登録機能の呼び出しを行います.
-- 登録する画像を引数で指定し、画像中で検出された顔を認識対象として登録します。
--
-- @function register
--
-- @tparam pux.webapi.Source content
--   顔検出を行う対象を指定します
--
-- @tparam[opt] table tags
--   識別画像に対するタグ情報
--
-- @return
--   登録した顔情報を@{PersonForFaceDetect}のリストで返します。　
--
-- @raise
--   - APIキーを設定せずに本メソッドを呼出場合に例外が発生します。
--     (API key is not set)
--   - タグの形式が誤っている場合に例外が発生します。
--     (Not supported tag type)
--
-- @usage
--   faceu   = require 'pux.webapi.faceu'
--   content = require 'pux.webapi.content'
--
--   api = faceu.new { api_key = "XXXXXXX..." }
--
--   -- ローカルファイルで登録を行う場合
--   api:register( content.file( "sample.jpg"))
--
--   -- 外部のリソースを指定して登録を行う場合
--   api:register( content.url( "http://case.of.example/dummy.jpg"))
--
function FaceU:register( content, tags)
  local args
  local resp

  args = create_common_arguments( self)

  args[#args+1] = { "mode", "register" }

  if type(tags) == "table" or type(tags) == "string" then
    append_tag_info( args, tags)

  elseif tags == nil then
    -- Nothing

  else
    error( "Not supported tag type.")
  end

  if content.type == "URL" then
    args[#args+1] = { "imageURL", url.escape( content.url) }

    content = nil
  end

  resp = inject( self, args, content)

  return parse_face_detect_response( resp)
end

---
-- 識別機能の呼び出しを行います.
-- 識別対象の画像を引数で指定し、登録された人物が存在するか検査します
--
-- @function who
--
-- @tparam pux.webapi.Source content
--   識別を行う対象を指定します
--
-- @tparam[opt] table tags
--   識別画像に対するタグ情報
--
-- @return
--   識別した顔情報を@{PersonForFaceIdentify}のリストで返します。　
--
-- @raise
--   - APIキーを設定せずに本メソッドを呼出場合に例外が発生します。
--     (API key is not set)
--   - 識別対象の指定形式が誤っている場合に例外が発生します。
--     (Illeagal condition info)
--
-- @usage
--   faceu   = require 'pux.webapi.faceu'
--   content = require 'pux.webapi.content'
--
--   api = faceu.new { api_key = "XXXXXXX..." }
--
--   -- ローカルファイルで識別を行う場合
--   api:who( content.file( "sample.jpg"))
--
--   -- ローカルファイルを用いて、タグ vip が付与されている人物の識別を行う場合
--   api:who( content.file( "sample.jpg"), "vip")
--
--   -- ローカルファイルを用いて、タグgroupの値が"team-A"かつ、タグvipが付与さ
--   -- れている人物の識別を行う場合
--   api:who( content.file( "sample.jpg"), { group = "team-A", "vip"})
--
function FaceU:who( content, tags)
  local args
  local resp

  args = create_common_arguments( self)

  args[#args+1] = { "mode", "verify" }

  append_tag_cond( args, tags)

  if content.type == "URL" then
    args[#args+1] = { "imageURL", url.escape( content.url) }

    content = nil
  end

  resp = inject( self, args, content)

  return parse_face_identify_response( resp)
end

---
-- 顔情報の登録解除機能の呼び出しを行います.
-- 登録解除する顔情報を指定する為の条件を引数で指定します。
--
-- @function unregister
--
-- @tparam[opt] ?|table|string|number cond
--   登録を解除する顔情報を指定する為の条件を指定します。
--
-- @treturn |table|bool|
--   登録を解除した結果を返します。
--
-- @raise
--   - APIキーを設定せずに本メソッドを呼出場合に例外が発生します。
--     (API key is not set)
--   - 登録解除対象の指定形式が誤っている場合に例外が発生します。
--     (Illeagal condition info)
--
-- @usage
--   faceu = require 'pux.webapi.faceu'
--   api   = faceu.new { api_key = "XXXXXXX..." }
--
--   -- 指定したfaceIdの顔情報の登録を解除する場合
--   -- (削除できた場合はtrue,できなかった場合はfalseを返します)
--   result = api:unregister( 10)
--
--   -- 指定したfaceIdの顔情報の登録を解除する場合(複数同時指定)
--   -- (削除されたfaceIdのリストを返します)
--   result = api:unregister { 10, 20}
--
--   -- タグvipが付与されている顔情報の登録を解除する場合
--   -- (削除されたfaceIdのリストを返します)
--   result = api:unregister( "vip")
--
--   -- タグgroupの値が"team-A"かつgenderが"male"である顔情報の登録を解除
--   -- (削除されたfaceIdのリストを返します)
--   result = api:unregister{
--       group  = "team-A",
--       gender = "male"        
--   }
--
--   -- 全登録情報を削除する場合
--   -- (削除できた場合はtrue,できなかった場合はfalseを返します)
--   result = api:unregister()
--
function FaceU:unregister( cond)
  local ret
  local args
  local resp
  local i
  local val

  ret  = {}

  args = {
    { "mode", "delete" }
  }

  if cond == nil then
    args[#args+1] = { "faceId", "ALL" }
  else
    append_cond_info( args, cond)
  end

  resp = inject( self, args)

  if type(cond) == "table" or type(cond) == "string" then
    if resp.deleteInfo then
        for i,val in ipairs( resp.deleteInfo) do
          if val.faceId then
            ret[#ret+1] = val.faceId
          end
        end
    end

  else
    ret = (resp.status == "success")
  end
  
  return ret
end

---
-- 登録情報読み出し機能の呼び出しを行います.
--
-- @function list
--
-- @tparam[opt] ?|table|string|int cond
--   取得する情報の条件を指定します。
--
-- @return
--   取得した顔情報のリストを取得します。
--
-- @raise
--   - APIキーを設定せずに本メソッドを呼出場合に例外が発生します。
--     (API key is not set)
--   - 取得対象の指定形式が誤っている場合に例外が発生します。
--     (Illeagal condition info)
--
-- @usage
--   faceu = require 'pux.webapi.faceu'
--   api   = faceu.new { api_key = "XXXXXXX..." }
--
--   -- すべての登録情報を取得する場合
--   result = api:list()
--
--   -- fcaeIdが10番と20番の登録情報を取得する場合
--   result = api:list{ 10, 20 }
--
--   -- タグvipが付与されている登録情報を取得する場合
--   result = api:list( "vip")
--
--   -- タグgroupの値がteam-Bかつ、タグvipが付与されている登録情報を取得する
--   -- 場合。
--   result = api:list{ group = "team-B", "vip" }
--
function FaceU:list( cond)
  local ret
  local resp
  local i
  local val

  ret  = {}
  args = {
    { "mode", "list" }
  }

  if cond ~= nil then
    append_cond_info( args, cond)
  end

  resp = inject( self, args)

  if resp.listInfo then
    for i,val in ipairs(resp.listInfo) do
      ret[#ret+1] = convert_to_who_info( val)
    end
  end

  return ret
end

---
-- タグの削除を行います
--
-- @function remove_tag
--
-- @tparam ?|table|string|int cond
--   タグ削除の対象指定条件を指定します。
--
-- @tparam ?|string|table keys
--   削除するタグ名を指定します。
--
-- @raise
--   - APIキーを設定せずに本メソッドを呼出場合に例外が発生します。
--     (API key is not set)
--   - タグ削除対象の指定形式が誤っている場合に例外が発生します。
--     (Illeagal condition info)
--   - 削除するタグ名の指定形式が誤っている場合に例外が発生します。
--     (Illeagal keys format)
--
-- @return
--   戻り値はありません。
--
-- @usage
--   faceu = require 'pux.webapi.faceu'
--   api   = faceu.new { api_key = "XXXXXXX..." }
--
--   -- すべての登録情報からタグgroupを取り除く場合
--   api:remove_tag( nil, "group")
--
--   -- fcaeIdが10番の登録情報からタグgroupとgenderを取り除く場合
--   api:remove_tag( 10, { "group", "gender" })
--
function FaceU:remove_tag( cond, keys)
  local args
  local key
  local val
  local resp

  args = {
    { "mode",   "tag_edit" },
    { "method", "delete" }
  }

  if cond == nil then
    args[#args+1] = { "faceId", "ALL" }
  else
    append_cond_info( args, cond)
  end

  if type(keys) == "string" then
    args[#args+1] = { "tag", keys }

  elseif type(keys) == "table" then
    for key,val in pairs(keys) do
      if type(key) ~= "number" then
        error( "Illeagal keys format.")
      end

      if type(val) ~= "string" then
        error( "Illeagal keys format.")
      end

      args[#args+1] = { "tag", val }
    end

  else
    error( "Illeagal keys format.")
  end

  inject( self, args)
end

---
-- タグの設定を行います
--
-- @function put_tag
--
-- @tparam ?|nil|table|string|number cond
--   タグ設定対象指定条件を指定します。
--
-- @tparam ?|table|string tags
--   設定するタグを指定します。
--
-- @return
--   戻り値はありません。
--
-- @raise
--   - APIキーを設定せずに本メソッドを呼出場合に例外が発生します。
--     (API key is not set)
--   - タグ設定対象の指定形式が誤っている場合に例外が発生します。
--     (Illeagal condition info)
--   - タグの形式が誤っている場合に例外が発生します。
--     (Not supported tag type)
--
function FaceU:put_tag( cond, tags)
  local args

  args = {
    { "mode",   "tag_edit" },
    { "method", "add" }
  }

  if cond == nil then
    args[#args+1] = { "faceId", "ALL" }
  else
    append_cond_info( args, cond)
  end

  append_tag_info( args, tags)

  inject( self, args)
end

--- FaceUインスタンスの生成
--
-- @function pux.web.api.FaceU.new
--
-- @tparam[opt] table opt
--   検出機能のオプション指定パラメータを指定できます。
--   このクラスの持つフィールドと同じ項目の設定が可能です。
--
-- @treturn pux.webapi.FaceU
--   FaceUオブジェクトを返します。
--
function FaceU.new( opt)
  local ret
  local set

  ret = {}
  opt = opt or {}
  set = function(name) ret[name] = opt[name] or FaceU[ "default_" .. name] end

  set( "service_url")
  set( "agent_name")
  set( "api_key")
  set( "face_size")
  set( "parts_detect")
  set( "blink")
  set( "age")
  set( "gender")
  set( "angle")
  set( "smile")
  set( "funny_option")

  setmetatable( ret, FaceU)

  return ret;
end

return FaceU

-- vi:set ts=2 sw=2 et:
