#! /usr/bin/lua

--[[

 PUX API interface library for Lua

    Copyright (C) 2015 PUX Corporation. All rights reserved.

--]]

local ltn12  = require 'ltn12'
local source = require 'pux.webapi.source'

---
-- PUX WebAPI用コンテンツの生成インタフェースを提供するモジュールです。
--
-- @module pux.webapi.content
--
-- @usage
--   content = require 'pux.webapi.content'
--   src1 = content.file( "sample.jpg")
--   src2 = content.blob( data, "image/jpeg")
--

local content = {}

---
-- バイナリデータラッピング関数
--
-- @function blob
--
-- @tparam string src
--   コンテンツそのものバイナリデータ
--
-- @tparam string mime_type
--   指定したバイナリデータの種別をMIME Media typeの形式で指定します。
--
-- @treturn pux.webapi.Source
--   バイナリデータをラッピングしたSourceオブジェクト
-- @raise
--   - mime_typeの指定が無い場合に例外が発生します
--     (MIME media type is not specified)
--
-- @usage
--   -- 変数dataに入っているデータを、JPEGデータと見なしてラッピングします
--   content = require 'pux.webapi.content'
--   src = content.blob( data, "image/jpeg")
--
function content.blob( src, mime_type)
  local ret

  if not mime_type then
    error( "MIME media type is not specified.")
  end

  ret = {
    type   = "OCTET-STREAM",

    header = {
      ["Content-Type"]   = mime_type,
      ["Content-Length"] = #src
    },

    source = ltn12.source.string( src)
  }

  setmetatable( ret, source)

  return ret
end

--- 
-- ローカルファイルラッピング関数
--
-- @function file
--
-- @tparam string src
--   コンテンツとして使用するファイルへのパスを指定します
--
-- @tparam[opt] string mime_type
--  指定したバイナリデータの種別をMIME Media typeの形式で指定します。
--  このパラメータは省略可能です。省略した場合は、ファイルの拡張子を元に
--  メディアタイプを自動的に判断します
--
-- @treturn pux.webapi.Source
--   ローカルファイルをラッピングしたテーブル。
--
-- @raise
--   ファイル名からメディアタイプが推測できなかった場合に例外が発生します。
--
-- @usage
--    -- ローカルファイル"sample.jpg"をラッピングします
--    content = require 'pux.webapi.content'
--    src = content.file( "sample.jpg")
--
function content.file( src, mime_type)
  local fp
  local ext
  local size
  local ret

  fp   = io.open( src, "rb")
  size = fp:seek( "end")

  if not mime_type then
    ext = src:match( "%.%w+$"):lower()

    mime_type =
      ext == ".jpg" and "image/jpeg" or
      ext == ".gif" and "image/gif" or
      ext == ".png" and "image/png" or
      ext == ".tiff" and "image/tiff" or
      error( "unknown media type of '" .. src .. "'.")
  end

  ret  = {
    type   = "FILE",

    header = {
      ["Content-Type"]   = mime_type,
      ["Content-Length"] = size
    },

    source = ltn12.source.file( fp),
    file   = fp
  }

  setmetatable( ret, source)

  fp:seek( "set")

  return ret
end

---
-- 外部リソースURLラッピング関数
--
-- @tparam string src
--   コンテンツとして使用する外部リソースへのURLを指定します。
--
-- @treturn pux.webapi.Source
--   URLをラッピングしたテーブル。
--
-- @usage
--    -- 外部リソースをラッピングします
--    content = require 'pux.webapi.content'
--    src = content.url( "http://sample.example/test?no=20")
--
function content.url( src)
  local ret

  ret = {
    type = "URL",
    url  = src
  }

  setmetatable( ret, source)

  return ret
end

return content
