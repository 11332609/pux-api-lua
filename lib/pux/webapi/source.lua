#! /usr/bin/lua

--[[

 PUX API interface library for Lua

    Copyright (C) 2015 PUX Corporation. All rights reserved.

--]]


---
-- コンテンツのソースを抽象化するクラスです。
-- 現時点では何も実装はありません。とりあえず将来何か思いつくかもしれないので
-- クラス化しています。
-- インスタンスの生成処理は@{pux.webapi.content}モジュールで記述します。
--
-- @classmod pux.webapi.Source
--
local Source = {
  type   = nil,
  header = nil
}
Source.__index = Source

---
-- @field type
--   種別が設定されています。
--

---
-- @field header
--   HTTPリクエストの送出が必要な際に付与するヘッダのリストが設定されます。
--

return Source
